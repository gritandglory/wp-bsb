<?php 
/**
 * Adds JSON to <head>
 * 
 */

add_action('wp_head', 'json_ld_name');

function json_ld_name()
{
  // Open script
  $html = '<script type="application/ld+json">';

    $html .= '{';
      $html .= '"@context": "http://schema.org",';
      $html .= '"@type": "WebSite",';
      $html .= '"name": "'. get_bloginfo('name') . '",';
      $html .= '"url": "' . home_url() . '"';
    $html .= '}';

    $html .= '{';
      $html .= '"@context" : "http://schema.org",';
      $html .= '"@type" : "Organization",';
      $html .= '"name" : "'. get_bloginfo('name') . '",';
      $html .= '"url" : "' . home_url() . '",';
      $html .= '"sameAs" : [';
        $html .= '"https://www.facebook.com/budgetsavvybride",';
        $html .= '"https://www.twitter.com/savvybride",';
        $html .= '"http://www.pinterest.com/savvybride"';
        $html .= '"http://www.instagram.com/savvybride"';
        $html .= '"https://www.bloglovin.com/blogs/budget-savvy-bride-830303"';
      $html .= ']';
    $html .= '}';

     $html .= '{';
      $html .= '"@context": "http://schema.org",';
      $html .= '"@type": "WebSite",';
      $html .= '"url": "' . home_url() . '",';

      $html .= '"potentialAction": {';
        $html .= '"@type": "SearchAction",';
        $html .= '"target": "' . home_url() . '/?s={search_term_string}",';
        $html .= '"query-input": "required name=search_term_string"';
      $html .= '}';
    $html .= '}';


  // Close script
  $html .= '</script>';

  echo $html;
}

add_action('wp_head', 'json_ld_article');

function json_ld_article()
{
  // Only on single posts
  if ( is_single() )
  {
    // We need access to the post
    global $post;
    setup_postdata($post);

    // Variables
    $logo = get_template_directory_uri() . '/assets/images/BSB-logo.png';
    $logo_width = 300;
    $logo_height = 60;
    $excerpt = get_the_excerpt();
    $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');

    // Open script
    $html = '<script type="application/ld+json">';

      $html .= '{';
        $html .= '"@context": "http://schema.org",';
        $html .= '"@type": "BlogPosting",';

        $html .= '"mainEntityOfPage": {';
          $html .= '"@type":"WebPage",';
          $html .= '"@id": "' . get_the_permalink() . '"';
        $html .= '},';

        $html .= '"headline": "' . get_the_title() . '",';

        if ( $image )
        {
          $html .= '"image": {';
            $html .= '"@type": "ImageObject",';
            $html .= '"url": "' . $image[0] . '",';
            $html .= '"height": ' . $image[1] . ',';
            $html .= '"width": ' . $image[2];
          $html .= '},';
        }

        $html .= '"datePublished": "' . get_the_date('c') . '",';
        $html .= '"dateModified": "' . get_the_modified_date('c') . '",';

        $html .= '"author": {';
          $html .= '"@type": "Person",';
          $html .= '"name": "' . get_the_author() . '"';
        $html .= '},';

        $html .= '"publisher": {';
          $html .= '"@type": "Organization",';
          $html .= '"name": "' . get_bloginfo('name') . '",';
          $html .= '"logo": {';
            $html .= '"@type": "ImageObject",';
            $html .= '"url": "' . $logo . '",';
            $html .= '"width": ' . $logo_width . ',';
            $html .= '"height": ' . $logo_height;
          $html .= '}';
        $html .= '}';

        if ( $excerpt ) $html .= ', "description": "' . esc_attr($excerpt) . '"';
      $html .= '}';

    // Close script
    $html .= '</script>';

    echo $html;
  }
}

