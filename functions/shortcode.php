<?php
/**

Shortcodes for the website. 

*/


// Short code for wedding budget circles

// Add Shortcode


function custom_shortcode( $atts , $content = null ) {

	$url = get_site_url();

	return

	'<div class="return-to-tips">

	<hr/>

	<div class="return-image large-4 columns">

	<a href="' . $url . '/wedding-budget-tips/">

	<img class="alignnone size-full wp-image-82918" src="' . $url . '/wp-content/uploads/2018/09/BUDGET-TIPS.png" alt="" width="350" height="350" />
	</a>
		
	</div>

	<div class="return-content large-8 columns">

	<h2><strong>Want more wedding budget tips?</strong></h2>

	<h4><span style="color: #f37d90;"><strong>Get our best money-saving tricks &amp; hacks!</strong></span></h4>

	<p>Click the button below to read more. <a href="' . $url . '/wedding-budget-tips/" target="_blank" rel="noopener">wedding budget tips</a>!</p>
		
			<a class="btn btn-coral btn-small-left" href="' . $url . '/wedding-budget-tips/">Get More Tips</a>
		</div>
		
		<hr/>
		</div>'
	;

}
add_shortcode( 'wbt', 'custom_shortcode' );



function offer_shortcode( $atts = '' ) {
    $value = shortcode_atts( array(
        'code' => 'XXXX',
        'url' => '#',
        'position' => 'left',
    ), $atts );
 
    return '<div class="deals-code-page '.$value['position'].'">

				<div class="offer-code">

					<h4>'.$value['code'].'</h4>

				</div>

				<div class="offer-link">

					<a class="btn btn-coral btn-medium-'.$value['position'].'" href="'.$value['url'].'">Shop Now</a>

				</div>

			</div>

			<div class="clearfix"></div>
			';
}


add_shortcode( 'offer', 'offer_shortcode' );