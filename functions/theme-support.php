<?php

// Adding WP Functions & Theme Support
function joints_theme_support() {

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );

	// Default thumbnail size
	set_post_thumbnail_size(125, 125, true);

	// Default image sizes
	add_image_size( 'article-thumbnail', 450, 450, array( 'center', 'center' ) );
	//add_image_size( 'article-thumbnail_x2', 700, 700, array( 'center', 'center' ) );


	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );

	// Add Support for WP Controlled Title Tag
	add_theme_support( 'title-tag' );

	// Add HTML5 Support
	add_theme_support( 'html5',
	         array(
	         	'comment-list',
	         	'comment-form',
	         	'search-form',
	         )
	);

	add_theme_support( 'custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array( 'site-title', 'site-description' ),
	) );

	// Adding post format support
	 add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
		)
	);

	// Set the maximum allowed width for any content in the theme, like oEmbeds and images added to posts.
	$GLOBALS['content_width'] = apply_filters( 'joints_theme_support', 1200 );

} /* end theme support */

add_action( 'after_setup_theme', 'joints_theme_support' );


add_theme_support( 'featured-content', array(
  'featured_content_filter' => 'mytheme_get_featured_content',
));

function get_featured_content( $num = 1 ) {
  global $featured;
  $featured = apply_filters( 'featured_content', array() );

  if ( is_array( $featured ) || $num >= count( $featured ) )
    return true;

  return false;
}

function add_avatar_alt_tag($text)
{
$alt = get_the_author_meta( 'display_name' );
$text = str_replace('alt=\'\'', 'alt=\''.$alt.'\' title=\''.$alt.'\'',$text);
return $text;
}
add_filter('get_avatar','add_avatar_alt_tag');


function add_post_type_to_feed( $query ) {
if ( is_home() && $query->is_main_query() )
$query->set( 'post_type', array( 'post', 'slideshow') );
return $query;
}
add_filter( 'pre_get_posts', 'add_post_type_to_feed' );

function miniExcerpt($value) {

	$x =  substr(get_the_excerpt(),0, $value);
	echo $x . '...';
}