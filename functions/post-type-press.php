<?php

function custom_post_press() {
	register_post_type( 'press_type',
		array('labels' => array(
			'name' => __('Press', 'jointswp'),
			'singular_name' => __('Press Post', 'jointswp'),
			'all_items' => __('All Press Posts', 'jointswp'),
			'add_new' => __('Add New Press', 'jointswp'),
			'add_new_item' => __('Add New Press Type', 'jointswp'),
			'edit' => __( 'Edit', 'jointswp' ),
			'edit_item' => __('Edit Post Types', 'jointswp'),
			'new_item' => __('New Post Type', 'jointswp'),
			'view_item' => __('View Post Type', 'jointswp'),
			'search_items' => __('Search Post Type', 'jointswp'),
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'),
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'),
			'parent_item_colon' => ''),
			'description' => __( 'This is the example custom post type', 'jointswp' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-testimonial',
			'rewrite'	=> array( 'slug' => 'press', 'with_front' => false ),
			'has_archive' => 'press',
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields',  'sticky')
	 	)
	);


	register_taxonomy_for_object_type('category', 'press_type');

	register_taxonomy_for_object_type('post_tag', 'press_type');

}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_press');


