<?php

// let's create the function for the custom type
function custom_post_printable() {
	// creating (registering) the custom type
	register_post_type( 'printable_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Printable', 'wpbsb'), /* This is the Title of the Group */
			'singular_name' => __('Printable Post', 'wpbsb'), /* This is the individual type */
			'all_items' => __('All Printable Posts', 'wpbsb'), /* the all items menu item */
			'add_new' => __('Add New OPrintableffer', 'wpbsb'), /* The add new menu item */
			'add_new_item' => __('Add New Offer Type', 'wpbsb'), /* Add New Display Title */
			'edit' => __( 'Edit', 'wpbsb' ), /* Edit Dialog */
			'edit_item' => __('Edit Printable Types', 'wpbsb'), /* Edit Display Title */
			'new_item' => __('New Printable Type', 'wpbsb'), /* New Display Title */
			'view_item' => __('View Printable Type', 'wpbsb'), /* View Display Title */
			'search_items' => __('Search Printable Type', 'wpbsb'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found in the Database.', 'wpbsb'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash', 'wpbsb'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example custom post type', 'wpbsb' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 6, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-money', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'printable-type', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'printable-type', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */



}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_printable');

	// now let's add custom categories (these act like categories)
    register_taxonomy( 'custom_cat-printable',
    	array('printable_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */
    		'labels' => array(
    			'name' => __( 'Offer Printables', 'wpbsb' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Offer Printable', 'wpbsb' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Offer Printables', 'wpbsb' ), /* search title for taxomony */
    			'all_items' => __( 'All Offer Printables', 'wpbsb' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Offer Printable', 'wpbsb' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Offer Printable:', 'wpbsb' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Offer Printable', 'wpbsb' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Offer Printable', 'wpbsb' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Offer Printable', 'wpbsb' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Offer Printable Name', 'wpbsb' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'custom-slug' ),
    	)
    );

