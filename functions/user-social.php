<?php


if( ! function_exists('add_contact_fields') ) : // Check if the function add_custom_fields does not exist
		function add_contact_fields($profile_fields) { // Add contact fields to WordPress
			$profile_fields['pinterest'] = 'Pinterest Username';
			$profile_fields['googleplus'] = 'Google+ Profile URL';
			$profile_fields['instagram'] = 'Instagram Username';
			$profile_fields['twitter'] = 'Twitter Username';
			$profile_fields['facebook_url'] = 'Facebook Username';
			$profile_fields['blog_lovin'] = 'Blog Lovin URL';
			$profile_fields['youtube'] = 'YouTube Channel';
			$profile_fields['rss_feed'] = 'RSS Feed URL';
			$profile_fields['blog_lovin'] = 'Blog Lovin URL';
			
			return $profile_fields;
		}
		add_filter('user_contactmethods', 'add_contact_fields'); // Apply the add_contact_fields function to user_contactmethods 
	endif;
	
if(function_exists('add_contact_fields')) :
	function user_social_links( $atts ) {

		extract( shortcode_atts(
			array(
				'username' => '',
			), $atts )
		);

		$username = $atts['username'];
		$user = get_user_by( 'login', $username  );
		$rss = $user->rss_feed;
		$email = $user->user_email;
		$facebook = $user->facebook_url;
		$twitter = $user->twitter;
		$googleplus = $user->googleplus;
		$instagram = $user->instagram;
		$youtube = $user->youtube;
		$pinterest = $user->pinterest;
		$bloglovin = $user->blog_lovin;
		$buttons = '<ul class="s-btn">';


		if(!empty($rss)){$buttons .= '<li><a target="_blank" href="'.$rss.'"><i class="fa fa-lg fa-rss"></i></a></li>';};
		if(!empty($email)){$buttons .= '<li><a target="_blank" href="mailto:'.$email.'"><i class="far fa-envelope-open"></i></i></a></li>';};
		if(!empty($facebook)){$buttons .= '<li><a target="_blank" href="http://www.facebook.com/'.$facebook.'"><i class="fab fa-facebook"></i></a></li>';};
		if(!empty($twitter)){ $buttons .= '<li><a target="_blank" href="http://twitter.com/'.$twitter.'"><i class="fab fa-twitter"></i></a></li>';};
		if(!empty($googleplus)){$buttons .= '<li><a target="_blank" href="https://plus.google.com/+'.$googleplus.'"><i class="fab fa-google-plus-square"></i></a></li>';};
		if(!empty($instagram)){$buttons .= '<li><a target="_blank" href="http://www.instagram.com/'.$instagram.'"><i class="fab fa-instagram"></i></a></li>';};
		if(!empty($youtube)){$buttons .= '<li><a target="_blank" href="'.$youtube.'"><i class="fab fa-youtube"></i></a></li>';};
		if(!empty($pinterest)){$buttons .= '<li><a target="_blank" href="http://www.pinterest.com/'.$pinterest.'"><i class="fab fa-pinterest"></i></a></li>';};
		if(!empty($bloglovin)){$buttons .= '<li><a target="_blank" href="'.$bloglovin.'"><i class="fas fa-heart"></i></a></li>';};
		$buttons .= '</ul><div class="clear"></div>';
		return $buttons;
	}
	add_shortcode( 'social_links', 'user_social_links' );
	endif;