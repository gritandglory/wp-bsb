<?php



function myguten_enqueue() {
	wp_enqueue_script(
		'myguten-script',
		get_stylesheet_directory_uri() . '/assets/scripts/editor.js',
		array( 'wp-blocks', 'wp-dom-ready', 'wp-edit-post' ),
		filemtime( get_stylesheet_directory() . '/assets/scripts/' ),
		true
	);
}
add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );

/*
 *  Remove custom color palette
 */

add_theme_support( 'disable-custom-colors' );

/*
 *  Add custom colors
 */
add_theme_support( 'editor-color-palette', array(
	array(
		'name'  => __( 'Teal', 'wp-bsb' ),
		'slug'  => 'teal',
		'color'	=> '#60AAAF',
	),
	array(
		'name'  => __( 'Coral', 'wp-bsb' ),
		'slug'  => 'coral',
		'color' => '#E48391',
	),
	array(
		'name'  => __( 'Light gray', 'wp-bsb' ),
		'slug'  => 'light-gray',
		'color' => '#707070',
	),
	array(
		'name'  => __( 'Dark gray', 'wp-bsb' ),
		'slug'  => 'dark-gray',
		'color' => '#060606',
	),
	array(
		'name'  => __( 'White', 'wp-bsb' ),
		'slug'  => 'white',
		'color' => '#fff',
	),
) );