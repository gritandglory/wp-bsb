<?php
// Adds your styles to the WordPress editor
add_action( 'init', 'add_editor_styles' );
function add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/assets/styles/style.min.css' );
}


// Adds new options to the drop down menu

function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');



/*
* Callback function to filter the MCE settings
*/

function my_mce_before_init_insert_formats( $init_array ) {
    $style_formats = array(
        array(
            'title' => 'Buttons Turquoise',
            'items' => array(
                array(
                    'title' => 'Small Left',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-small-left'
                ),
                array(
                    'title' => 'Medium Left',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-medium-left'
                ),
                array(
                    'title' => 'Large Left',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-large-left'
                ),
                array(
                    'title' => 'Small Center',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-small-center'
                ),
                array(
                    'title' => 'Medium Center',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-medium-center'
                ),
                array(
                    'title' => 'Large Center',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-large-center'
                ),
                array(
                    'title' => 'Small Right',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-small-right'
                ),
                array(
                    'title' => 'Medium Right',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-medium-right'
                ),
                array(
                    'title' => 'Large Right',
                    'selector' => 'a',
                    'classes' => 'btn btn-tq btn-large-right'
                ),

            ),
        ),
        array(
            'title' => 'Buttons Coral',
            'items' => array(
                array(
                    'title' => 'Small Left',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-small-left'
                ),
                array(
                    'title' => 'Medium Left',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-medium-left'
                ),
                array(
                    'title' => 'Large Left',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-large-left'
                ),
                array(
                    'title' => 'Small Center',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-small-center'
                ),
                array(
                    'title' => 'Medium Center',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-medium-center'
                ),
                array(
                    'title' => 'Large Center',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-large-center'
                ),
                array(
                    'title' => 'Small Right',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-small-right'
                ),
                array(
                    'title' => 'Medium Right',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-medium-right'
                ),
                array(
                    'title' => 'Large Right',
                    'selector' => 'a',
                    'classes' => 'btn btn-coral btn-large-right'
                ),

            ),
        ),
        array(
            'title' => 'Misc',
            'items' => array(
                array(
                    'title' => 'Divider',
                    'selector' => 'div',
                    'classes' => 'divider-line'
                ),
               array(
                    'title' => 'Round Circle',
                    'selector' => 'p,a,h1,h2,h3,h4,h5',
                    'classes' => 'round-circle'
                ),
            ),
        ),
        array(
            'title' => 'Heading Background',
            'items' => array(
                array(
                    'title' => 'H1',
                    'selector' => 'h1',
                    'classes' => 'h1bg'
                ),
                array(
                    'title' => 'H2',
                    'selector' => 'h2',
                    'classes' => 'h2bg'
                ),
                array(
                    'title' => 'H3',
                    'selector' => 'h3',
                    'classes' => 'h3bg'
                ),
                array(
                    'title' => 'H4',
                    'selector' => 'h4',
                    'classes' => 'h4bg'
                ),
                array(
                    'title' => 'H5',
                    'selector' => 'h5',
                    'classes' => 'h5bg'
                ),
                 array(
                    'title' => 'Paragraph',
                    'selector' => 'p',
                    'classes' => 'pbg'
                ),

            )
        )
    );

    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;
}

add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

/**
 * Remove empty <p> tag.
 *
 * @param type $content
 * @return type
 */
function pd_remove_unwanted_p($content){
    $content = force_balance_tags( $content );
    $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
    $content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
    return $content;
}
add_filter('the_content', 'pd_remove_unwanted_p', 20, 1);

/**
Remove <p> tags from around images
**/


function filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

add_filter('the_content', 'filter_ptags_on_images');

    // TinyMCE Text Color options
    function rvp_tbsb_mce_options( $init ) {
        $default_colours = '
            "000000", "Black",
            "993300", "Burnt orange",
            "333300", "Dark olive",
            "003300", "Dark green",
            "003366", "Dark azure",
            "000080", "Navy Blue",
            "333399", "Indigo",
            "333333", "Very dark gray",
            "800000", "Maroon",
            "FF6600", "Orange",
            "808000", "Olive",
            "008000", "Green",
            "008080", "Teal",
            "0000FF", "Blue",
            "666699", "Grayish blue",
            "808080", "Gray",
            "FF0000", "Red",
            "FF9900", "Amber",
            "99CC00", "Yellow green",
            "339966", "Sea green",
            "33CCCC", "Turquoise",
            "3366FF", "Royal blue",
            "800080", "Purple",
            "999999", "Medium gray",
            "FF00FF", "Magenta",
            "FFCC00", "Gold",
            "FFFF00", "Yellow",
            "00FF00", "Lime",
            "00FFFF", "Aqua",
            "00CCFF", "Sky blue",
            "993366", "Brown",
            "C0C0C0", "Silver",
            "FF99CC", "Pink",
            "FFCC99", "Peach",
            "FFFF99", "Light yellow",
            "CCFFCC", "Pale green",
            "CCFFFF", "Pale cyan",
            "99CCFF", "Light sky blue",
            "CC99FF", "Plum",
            "FFFFFF", "White"
        ';
        $custom_colours = '
            "70C2CA", "Sky (are you) blue?", 
            "9BD4DA", "Ocean Breeze", 
            "E9F4F6", "Splash of Blue", 
            "DCECED", "Pgazur", 
            "D54A7F", "Peach", 
            "F37D90", "My Peaches", 
            "F0BDB6", "The pinks",
            "F7DBD7", "Baby pinks",
            "343434", "Concrete",
            "D8D8D8", "Pavement",
            "F0BDB6", "Sun Pavement",
        ';
        $init['textcolor_map'] = '['.$default_colours.','.$custom_colours.']'; // build colour grid default+custom colors
        $init['textcolor_rows'] = 6; // enable 6th row for custom colours in grid
        return $init;
    }
    add_filter('tiny_mce_before_init', 'rvp_tbsb_mce_options');

        // Add custom Fonts to the Fonts list
    if ( ! function_exists( 'rvp_tbsb_mce_fonts_array' ) ) {
        function rvp_tbsb_mce_fonts_array( $initArray ) {
            $initArray['font_formats'] = 'Cookie=Cookie';
                return $initArray;
        }
    }
    add_filter( 'tiny_mce_before_init', 'rvp_tbsb_mce_fonts_array' );

    if ( ! function_exists( 'wpex_mce_buttons' ) ) {
        function wpex_mce_buttons( $buttons ) {
            array_unshift( $buttons, 'fontselect' ); // Add Font Select
            return $buttons;
        }
    }
    add_filter( 'mce_buttons_2', 'wpex_mce_buttons' );


