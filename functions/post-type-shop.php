<?php

function custom_shop_offers() {
	register_post_type( 'shop_type',
		array('labels' => array(
			'name' => __('Product', 'jointswp'),
			'singular_name' => __('Products', 'jointswp'),
			'all_items' => __('All Product Posts', 'jointswp'),
			'add_new' => __('Add New Product', 'jointswp'),
			'add_new_item' => __('Add New Product Type', 'jointswp'),
			'edit' => __( 'Edit', 'jointswp' ),
			'edit_item' => __('Edit Post Types', 'jointswp'),
			'new_item' => __('New Post Type', 'jointswp'),
			'view_item' => __('View Post Type', 'jointswp'),
			'search_items' => __('Search Post Type', 'jointswp'),
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'),
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'),
			'parent_item_colon' => ''),
			'description' => __( 'This is the example custom post type', 'jointswp' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-money',
			'rewrite'	=> array( 'slug' => 'shop', 'with_front' => true ),
			'has_archive' => 'shop',
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', )
	 	)
	);

}


	add_action( 'init', 'custom_shop_offers');


    register_taxonomy( 'custom_cat-shop',
    	array('shop_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */
    		'labels' => array(
    			'name' => __( 'Shop Categories', 'jointswp' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Shop Category', 'jointswp' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Shop Categories', 'jointswp' ), /* search title for taxomony */
    			'all_items' => __( 'All Shop Categories', 'jointswp' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Shop Category', 'jointswp' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Shop Category:', 'jointswp' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Shop Category', 'jointswp' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Shop Category', 'jointswp' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Shop Category', 'jointswp' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Shop Category Name', 'jointswp' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true,
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'products', 'with_front' => true ),
    	)
    );
