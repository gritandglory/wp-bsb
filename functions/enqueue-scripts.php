<?php
function site_scripts() {
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

    // Adding scripts file in the footer
    wp_enqueue_script( 'site-js', get_template_directory_uri() . '/assets/src/js/scripts-min.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );
    // Adding scripts file in the footer

    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/assets/scripts/js/init-foundation.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );




	//wp_enqueue_script( 'slick-js', get_template_directory_uri() . '/assets/slick/slick.js', array( 'jquery' ), filemtime(get_template_directory() . '/assets/scripts/js'), true );

    // Register main stylesheet
    //wp_enqueue_style( 'fonts-css', get_template_directory_uri() . '/assets/styles/fonts.css', array(), 'all' );

        // Register main stylesheet
    wp_enqueue_style( 'master-css', get_template_directory_uri() . '/assets/src/css/style.css', array(),  'all' );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/assets/src/css/foundation.css', array(), filemtime(get_template_directory() . '/assets/styles/'), 'all' );

    wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css?family=Cookie', false );



    wp_enqueue_style('font-awesome', 'https://assets.thebudgetsavvybride.com/fonts/fontawesome/css/all.css');

    // Comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);