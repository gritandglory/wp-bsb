<?php

function custom_slideshow() { 
	register_post_type( 'slideshow_type', 
		array('labels' => array(
			'name' => __('Slideshow', 'jointswp'), 
			'singular_name' => __('Slide', 'jointswp'),
			'all_items' => __('All Slides Posts', 'jointswp'), 
			'add_new' => __('Add New Slide', 'jointswp'), 
			'add_new_item' => __('Add New Slide Type', 'jointswp'), 
			'edit' => __( 'Edit', 'jointswp' ), 
			'edit_item' => __('Edit Post Types', 'jointswp'), 
			'new_item' => __('New Post Type', 'jointswp'), 
			'view_item' => __('View Post Type', 'jointswp'), 
			'search_items' => __('Search Post Type', 'jointswp'), 
			'not_found' =>  __('Nothing found in the Database.', 'jointswp'), 
			'not_found_in_trash' => __('Nothing found in Trash', 'jointswp'), 
			'parent_item_colon' => ''), 
			'description' => __( 'This is the example custom post type', 'jointswp' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, 
			'menu_icon' => 'dashicons-format-gallery', 
			'rewrite'	=> array( 'slug' => 'slideshow', 'with_front' => true ), 
			'has_archive' => 'slideshow',
			'capability_type' => 'post',
			'hierarchical' => true,
			'supports' => array( 'title', 'editor', 'thumbnail',  )
	 	) 
	); 


	register_taxonomy_for_object_type('category', 'slideshow_type');

	register_taxonomy_for_object_type('post_tag', 'slideshow_type');
	
} 

	
	add_action( 'init', 'custom_slideshow');


    register_taxonomy( 'custom_cat-slideshow', 
    	array('slideshow_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
    	array('hierarchical' => true,     /* if this is true, it acts like categories */             
    		'labels' => array(
    			'name' => __( 'Slideshow Categories', 'jointswp' ), /* name of the custom taxonomy */
    			'singular_name' => __( 'Slideshow Category', 'jointswp' ), /* single taxonomy name */
    			'search_items' =>  __( 'Search Slideshow Categories', 'jointswp' ), /* search title for taxomony */
    			'all_items' => __( 'All Slideshow Categories', 'jointswp' ), /* all title for taxonomies */
    			'parent_item' => __( 'Parent Slideshow Category', 'jointswp' ), /* parent title for taxonomy */
    			'parent_item_colon' => __( 'Parent Slideshow Category:', 'jointswp' ), /* parent taxonomy title */
    			'edit_item' => __( 'Edit Slideshow Category', 'jointswp' ), /* edit custom taxonomy title */
    			'update_item' => __( 'Update Slideshow Category', 'jointswp' ), /* update title for taxonomy */
    			'add_new_item' => __( 'Add New Slideshow Category', 'jointswp' ), /* add new title for taxonomy */
    			'new_item_name' => __( 'New Slideshow Category Name', 'jointswp' ) /* name title for taxonomy */
    		),
    		'show_admin_column' => true, 
    		'show_ui' => true,
    		'query_var' => true,
    		'rewrite' => array( 'slug' => 'slides', 'with_front' => true ), 
    	)
    );   
