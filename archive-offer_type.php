<?php
/**
 * Displays archive pages if a speicifc template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>
    <div class="page-title-container">
        <h1>Wedding Deals</h1>
    </div>
    <div class="content">
        <div class="row">
            <main class="main small-12 large-9 columns" role="main" style="float: none; margin: 0 auto;">
                <div class="top-text">
                    <p>The Budget Savvy Bride works with some of the most budget friendly and amazing vendors/merchants
                        in the wedding industry. We take pride in the partnerships we have with our vendors to bring our
                        readers exclusive discounts, wedding deals, and savings that you won't find anywhere else! We've
                        done the legwork for you so you can shop with confidence and score a great deal on just about
                        anything you need for your big day. Check out our list of exclusive wedding deals below!</p>
                    <p>Bookmark this page, and be sure to visit often– we update these wedding deals a few times per
                        week!!</p>
                    <button class="btn btn-medium-left btn-coral" type="button" data-toggle="offer-dropdown"
                            data-close-on-click="true" style="width: 100%;margin-top: 1rem;">Search for offers by
                        category
                    </button>
                    <div class="dropdown-pane" id="offer-dropdown" data-dropdown data-auto-focus="true">
						<?php
						/*
						* Loop through Categories and Display Posts within
						*/
						$post_type = 'offer_type';
						// Get all the taxonomies for this post type
						$taxonomies = get_object_taxonomies(array('post_type' => $post_type)); ?>

						<?php foreach ($taxonomies as $taxonomy) :
							// Gets every "category" (term) in this taxonomy to get the respective posts
							$terms = get_terms($taxonomy);
							foreach ($terms as $term) : ?>
                                <li><a href="#<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
							<?php endforeach;
						endforeach; ?>
                    </div>
                </div>
				<?php
				/*
				* Loop through Categories and Display Posts within
				*/
				$post_type = 'offer_type';
				// Get all the taxonomies for this post type
				$taxonomies = get_object_taxonomies(array('post_type' => $post_type)); ?>

				<?php foreach ($taxonomies as $taxonomy) :
					// Gets every "category" (term) in this taxonomy to get the respective posts
                    // Hide the post from Yoast Pro
					$terms = get_terms(['taxonomy' =>  $taxonomy, 'hide_empty' => true]);
					foreach ($terms as $term) : ?>

                        <h2 class="offer-category-title" id="<?php echo $term->slug; ?>"><?php echo $term->name; ?></h2>
						<?php
						$args = array('post_type' => $post_type, 'posts_per_page' => -1,  //show all posts
							'tax_query' => array(array('taxonomy' => $taxonomy, 'field' => 'slug', 'terms' => $term->slug,)));
						$posts = new WP_Query($args);
						if ($posts->have_posts()): ?>
							<?php while ($posts->have_posts()) : $posts->the_post(); ?>
                                <div class="offer-<?php the_ID(); ?> deals-container">
                                    <div class="deals-image">
                                        <a id="deal-link" href="<?php echo the_field('offer_url'); ?>">
											<?php
											$image = get_field('offer_image');
											if (!empty($image)): ?>
                                                <img src="<?php echo $image['sizes']['article-thumbnail']; ?>"
                                                     alt="<?php echo $image['alt']; ?>"/>
											<?php endif; ?>
                                            <span class="the-deal">
											<h2><?php the_field('the_offer'); ?></h2>
										</span>
                                        </a>
                                    </div>
                                    <div class="deal-content_desktop">
                                        <div class="deal-type">
                                            <h4><?php the_field('offer_type'); ?></h4>
                                        </div>
                                        <div class="deal-title">
                                            <h2><?php the_field('offer_title'); ?></h2>
                                        </div>
                                        <div class="deal-contents">
											<?php the_field('offer_text'); ?>
                                        </div>
                                        <div class="deals-code-area">
                                            <div class="offer-code">
                                                <h4><?php the_field('offer_code'); ?></h4>
                                            </div>
                                            <div class="offer-link">
                                                <a id="deal-link" class="btn btn-coral btn-medium-left"
                                                   href="<?php the_field('offer_url'); ?>">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="deal-content_mobile">
                                        <a href="<?php the_permalink() ?>">
                                            <div class="deal-type">
                                                <h4 class="deal-type_title"><?php the_field('offer_type'); ?></h4>
                                            </div>
                                            <div class="deal-title">
                                                <h2 class="deal-title_title"><?php the_field('offer_title'); ?></h2>
                                            </div>
                                            <div class="deal-mobile-comment">
                                                <p class="deal-mobile-comment_p"><?php the_field('offer_comment'); ?></p>
                                            </div>
                                            <div class="deal-offer">
                                                <h2 class="deal-offer_title"><?php the_field('the_offer'); ?></h2>
                                            </div>
                                        </a>
                                    </div>
                                </div>
							<?php endwhile; endif; ?>
					<?php endforeach;
				endforeach; ?>
            </main>
			<?php /*get_sidebar(); */ ?>
        </div>
    </div>
<?php get_footer(); ?>