<?php
/*
Template Name: Full Width (No Sidebar)
*/
get_header(); ?>
<div class="page-title-container">
    <h1 class="page-title"><?php the_title(); ?></h1>
</div>
<div class="content">
    <div class="row">
        <div class="full-width-content">

                    <main class="main small-12 large-9 columns" role="main" style="float: none; margin: 0 auto;">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <article id="page-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope
                             itemtype="http://schema.org/WebPage">
                        <section class="full-page-content" itemprop="articleBody">
							<?php the_content(); ?>
                        </section>
                    </article
				<?php endwhile; endif; ?>
            </main>
        </div>
    </div>
</div>
<?php get_footer(); ?>
