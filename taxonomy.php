
<?php
/**
 * Displays archive pages if a speicifc template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header(); ?>

<div class="shop-title-container">
	<h2>The Budget Savvy Bride Shop</h2>
</div>

	<div class="content-wrapper">

		<div class="row">

			<div class="inner-content">


				<aside class="large-3 columns">
					<?php echo facetwp_display( 'facet', 'categories' ); ?>
					<?php echo facetwp_display( 'facet', 'price' ); ?>
				</aside>
			    <main class="main large-9 columns" role="main">


		    	<?php echo facetwp_display( 'template', 'shop' ); ?>

			</main> <!-- end #main -->



	    </div> <!-- end #inner-content -->
	    </div> <!-- end #grid-container -->
	</div> <!-- end #content -->

<?php get_footer(); ?>