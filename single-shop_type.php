<?php
/**
 * The template for displaying all single posts and attachments
 */
get_header(); ?>

<div class="shop-title-container">

	<h1 class="shop-title">The Budget Savvy Bride Shop</h1>

</div>

	<div class="content">

		<div class="row">

		    <main id="shop-single" class="main large-12 columns" role="main">

		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<div class="product-<?php the_ID(); ?> product--container">

		 			<div class="medium-5 columns">
		 				<?php

							$image = get_field('product_image');

							if( !empty($image) ): ?>

								<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

						<?php endif; ?>

		 			</div>

		 			<div class="medium-7 columns">

						<div class="product-breadcrumb">

							<a class="" href="<?php echo get_site_url(); ?>/products/accessories/"><strong><i class="fas fa-angle-left"></i> Back to shop</strong></a>

						</div>

						<div class="product-content">
							<h2 class="product---name"><?php the_field('product_name'); ?></h2>
							<h5 class="shop---name"><?php the_field('shop_name'); ?> </h5>
							<h4 class="product---price">$ <?php the_field('product_price'); ?> </h4>
							<h3>Product Details</h3>
							<p><?php the_field('product_details'); ?></p>
							<a class="btn btn-medium-left btn-coral" href="<?php the_field('product_url'); ?>" target="_blank" >Shop Now</a>

						</div>

						<?php

							$postURL = urlencode(get_permalink());
							$postTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
							$postThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
							$twitterURL = 'https://twitter.com/intent/tweet?text='.$postTitle.'&amp;url='.$postURL;
							$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$postURL;
							$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$postURL.'&amp;media='.$postThumbnail[0].'&amp;description='.$postTitle;

						?>

						<div id="sls-buttons">

							<h4>Share</h4>

							<a target="_blank" href="<?php echo $facebookURL ?>" class="share-button facebook"><i class="fab fa-facebook"></i></a>

							<a target="_blank" href="<?php echo $twitterURL ?>" class="share-button twitter"><i class="fab fa-twitter"></i></a>

							<a target="_blank" href="<?php echo $pinterestURL ?>" class="share-button pinterest"><i class="fab fa-pinterest"></i></a>

						</div>

					</div>

	 			</div>

	 			<div class="clearfix"></div>

	 			 <?php get_template_part( 'parts/content', 'related-shop' ); ?>

	 			 <?php endwhile; else : ?>

	   			<?php get_template_part( 'parts/content', 'missing' ); ?>

	    		<?php endif; ?>

			</main>

		</div>

	</div>

<?php get_footer(); ?>
