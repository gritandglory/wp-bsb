<?php
/**
 * Displays archive pages if a speicifc template is not set. 
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */

get_header(); ?>

<div class="page-title-container">
	<h2><?php single_tag_title(); ?></h2>

</div>

	<div class="content">
	
		<div class="row">

		    <main class="main large-9 columns" role="main">
		    
	    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		 
				<?php get_template_part( 'parts/loop', 'archive' ); ?>
			    
			<?php endwhile; ?>	

				<?php joints_page_navi(); ?>
				
			<?php else : ?>
										
				<?php get_template_part( 'parts/content', 'missing' ); ?>
					
			<?php endif; ?>
	
		</main> 

		<?php get_sidebar(); ?>
    
    </div> 

</div> 

<?php get_footer(); ?>