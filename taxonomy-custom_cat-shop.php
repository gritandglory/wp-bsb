<?php
/**
 * Displays archive pages if a speicifc template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>
<div class="shop-title-container">
    <h1 class="shop-title">The Budget Savvy Bride Shop</h1>
</div>
<div class="content">
    <div class="row">
        <aside class="large-3 columns">
			<?php echo facetwp_display('facet', 'categories'); ?>
			<?php echo facetwp_display('facet', 'price'); ?>
        </aside>
        <main class="main large-9 columns" role="main">
            <div class="sort-search">
                <div class="count">
                    <span class="sort-search-text">Total Products: </span>
					<?php echo facetwp_display('counts'); ?>
                </div>
                <div class="per-page">
					<?php echo facetwp_display('per_page'); ?>
                </div>
                <div class="sort">
					<?php echo facetwp_display('sort'); ?>
                </div>
            </div>
			<?php echo facetwp_display('template', 'shop'); ?>
            <button class="fwp-load-more btn btn-coral btn-small-center">Load more</button>
        </main>
    </div>
</div>
<?php get_footer(); ?>