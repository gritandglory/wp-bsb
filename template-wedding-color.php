<?php
/*
Template Name: Wedding by Color
*/
get_header(); ?>
    <div class="page-title-container">
        <h1><?php single_post_title(); ?></h1>
    </div>
    <div class="content-wrapper">
        <div class="row">
            <main class="main small-12 large-9 columns" role="main">
                <div class="inner-content">
					<?php the_field('content_above') ?>
                    <div class="wedding-by-budget">
						<?php
						// check if the repeater field has rows of data
						if (have_rows('wedding_by_color')):
							$count = 0;
							$group = 0;
							while (have_rows('wedding_by_color')) : the_row();
								$color = get_sub_field('color_picker');
								$url = get_sub_field('url');
								if ($count % 10 == 0) {
									$group++;
									?>
                                    <div class="row ">
									<?php
								}
								?>
                                <a class="round-circle-wbc" href="<?php echo site_url(); ?>/<?php echo $url; ?>"
                                   style="background-color: <?php echo $color; ?>">
                                </a>
								<?php
								if ($count % 10 == 9) {
									?>
                                    </div>
									<?php
								}
								$count++;
							endwhile;
						else :
						endif; ?>
                    </div>
					<?php the_field('content_below') ?>
                    <div class="clearfix"></div>
                </div>
            </main>
			<?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>