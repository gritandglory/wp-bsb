<?php 
/**
 * The template for displaying all single posts and attachments
 */
get_header(); ?>



<div class="content">
	
	<div class="row">
		
		<main id="slideshow-single" class="main large-9 columns" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'slideshow' ); ?>

				<div class="clearfix"></div>

			<?php get_template_part( 'parts/content', 'related' ); ?>

			<?php endwhile; else : ?>

			<?php get_template_part( 'parts/content', 'missing' ); ?>

		<?php endif; ?>

	</main>

	<?php get_sidebar(); ?>

</div> 

</div> 

<?php get_footer(); ?>
