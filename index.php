<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 */

get_header(); ?>
<div class="page-title-container">
<h2>
	<?php single_post_title();?>

	</h2>

</div>
	<div class="content">
	
		<div class="row">
		
		    <main class="main small-12 large-9 columns" role="main">
		    
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<?php get_template_part( 'parts/loop', 'archive' ); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
		    </main> 
		    
		    <?php get_sidebar(); ?>

		</div> 
	</div> 

<?php get_footer(); ?>