<?php
/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>
<html class="no-js"  <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/assets/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/assets/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<meta name="google-site-verification" content="Ff1u2upFxCGi1490tnwQ97FC0wgzkLaGZ3sclWh9xzM" />
	<meta name="google-site-verification" content="yGigX9gP_EvN0e9nspY81h1lI73Ndnx34ikuxdMlMbo" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<!--<script>
		window.__savvy_addclassFont__=function(d){document.documentElement.classList.add(d.dataset.loadedClass)}
	</script>
	<link rel="preload" href="https://assets.thebudgetsavvybride.com/fonts/MuseoSans-300.woff2" as="font" onload="__savvy_addclassFont__(this)" type="font/woff2" crossorigin="anonymous" data-loaded-class="savvy-ms-300">
	<link rel="preload" href="https://assets.thebudgetsavvybride.com/fonts/MuseoSans-500.woff2" as="font" onload="__savvy_addclassFont__(this)" type="font/woff2" crossorigin="anonymous" data-loaded-class="savvy-ms-500">
	<link rel="preload" href="https://assets.thebudgetsavvybride.com/fonts/MuseoSans-500italic.woff2" as="font" onload="__savvy_addclassFont__(this)" type="font/woff2" crossorigin="anonymous" data-loaded-class="savvy-ms-500italic">
    <link rel="preload" href="https://assets.thebudgetsavvybride.com/fonts/MuseoSans-900.woff2" as="font" onload="__savvy_addclassFont__(this)" type="font/woff2" crossorigin="anonymous" data-loaded-class="savvy-ms-900">-->
	<?php wp_head(); ?>
	<?php get_template_part( 'parts/content', 'header-scripts' ); ?>
</head>
<body <?php body_class(); ?>>
	<div class="off-canvas-wrapper">
		<?php get_template_part( 'parts/content', 'offcanvas' ); ?>
		<div class="off-canvas-content" data-off-canvas-content>
			<header class="header fixed" role="banner">
				<?php get_template_part( 'parts/nav', 'offcanvas-topbar_v2' ); ?>
			</header>
			<div class="header-padding"></div>
			<div class="top-banner-ad">
				<div class="banner-container">
				</div>
			</div>