<?php
/*
Template Name: Image & Text with Repeater
*/
get_header(); ?>
    <div class="page-title-container">
        <h1><?php single_post_title(); ?></h1>
    </div>
    <div class="content-wrapper">
    <div class="row">
        <main class="main small-12 large-9 columns" role="main">
            <div class="inner-content">
                <div class="row">
                    <div class="medium-6 columns">
						<?php
						$image = get_field('top_left_image');
						if (!empty($image)): ?>
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
						<?php endif; ?>
                    </div>
                    <div class="medium-6 columns">
						<?php the_field('top_right_text'); ?>
                    </div>
                </div>
				<?php if (get_field('the_tip_repeater')): ?>
					<?php while (has_sub_field('the_tip_repeater')):
						$image = get_sub_field('tip_image');
						$link = get_sub_field('tip_url');
						?>
                        <div class="repeater-entry-container">
                            <div class="repeater-entry-thumbnail">
                                <a href="<?php echo $link; ?>">
                                    <img src="<?php echo $image['sizes']['article-thumbnail']; ?>"
                                         alt="<?php echo $image['alt'] ?>"/>
                                </a>
                            </div>
                            <div class="repeater-entry-content">
                                <h5 class="cat-title">
                                    <a class="cat-title-link"
                                       href="<?php echo $link ?>"><?php the_sub_field('tip_number_title'); ?></a>
                                </h5>
                                <h3 class="second-title"><?php the_sub_field('tip_title'); ?></h3>
                                <p class="max-80 entry-excerpt"><?php the_sub_field('tip_summary'); ?></p>
                                <div class="tip-footer">
                                    <a class="read-more" href="<?php echo $link ?>">Read Post </a>
                                </div>
                            </div>
                        </div>
					<?php endwhile; ?>
				<?php endif; ?>
                <div class="clearfix"></div>
            </div>
            </main>
			<?php get_sidebar(); ?>
        </main>
    </div>
<?php get_footer(); ?>