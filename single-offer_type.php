<?php
/**
 * The template for displaying all single posts and attachments
 */
get_header(); ?>
<div class="page-title-container">
    <h1>Wedding Deals</h1>
</div>

    <div class="row">
        <main id="offer-single" class="main" role="main">
            <div class="content">
            <div class="large-9 columns">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="offer-<?php the_ID(); ?> single-deals_container">
                        <div class="single-deals_image">
                            <a href="<?php echo the_field('offer_url'); ?>">

								<?php
								$image = get_field('offer_image');
								if (!empty($image)): ?>
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
								<?php endif; ?>
                                <span class="the-deal">

								<h2><?php the_field('the_offer'); ?></h2>

							</span>
                            </a>
                        </div>
                        <div class="single-deal_content">
                            <div class="single-deal_content_title">
                                <h2><?php the_field('offer_title'); ?></h2>
                            </div>
                            <div class="single-deal_content_text">
								<?php the_field('offer_text'); ?>
                            </div>
                            <div class="deals-code-area">
                                <div class="offer-code">
                                    <h4><?php the_field('offer_code'); ?></h4>
                                </div>
                                <div class="offer-link">
                                    <a class="btn btn-coral btn-medium-left" href="<?php the_field('offer_url'); ?>">Shop
                                        Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
				<?php endwhile; else : ?>
					<?php get_template_part('parts/content', 'missing'); ?>
				<?php endif; ?>
            </div>
            <div id="shop-sidebar" class="large-3 columns single-deal-category">
                <ul class="shop-cats">
                    <li class="shop-cat-title"><h4> Deals by category</h4></li>
					<?php
					/*
					* Loop through Categories and Display Posts within
					*/
					$post_type = 'offer_type';
					// Get all the taxonomies for this post type
					$taxonomies = get_object_taxonomies(
					        array(
					                'post_type' => $post_type
                            ));

					foreach ($taxonomies as $taxonomy) :
						// Gets every "category" (term) in this taxonomy to get the respective posts
						// Hide the post from Yoast Pro
						$terms = get_terms(['taxonomy' =>  $taxonomy, 'hide_empty' => true]);

						foreach ($terms as $term) : ?>

                            <li class="shop-cat">
                                <a class="shop-cat-link"
                                                    href="<?php get_site_url(); ?>/wedding-deals/#<?php echo $term->slug; ?>"
                                                    target="_blank"><?php echo $term->name; ?></a>
                            </li>
						<?php endforeach;
					endforeach; ?>
                </ul>
            </div>
            <div class="clearfix"></div>
			<?php get_template_part('parts/content', 'related-offer'); ?>
            </div>
        </main>
    </div>

<?php get_footer(); ?>
