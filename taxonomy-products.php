<?php
/**
 * Displays archive pages if a specific template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>
<div class="shop-title-container">
    <h1>The Budget Savvy Bride Shop</h1>
</div>
<div class="content-wrapper">
    <div class="row">
        <div class="inner-content">
            <aside class="large-3 columns">
				<?php echo facetwp_display('facet', 'categories'); ?>
				<?php echo facetwp_display('facet', 'price'); ?>
            </aside>
            <main class="main large-9 columns" role="main">
				<?php echo facetwp_display('template', 'shop'); ?>
            </main>
        </div>
    </div>
</div>
<?php get_footer(); ?>