<?php
/**
 * Template part for displaying an Home - 4 columns recent posts
 */
?>
<div class="recent-post-section">
    <div class="row">
        <h2 class="another-title"><?php the_field('recent_post_title'); ?></h2>
		<?php $the_query = new WP_Query('posts_per_page=4'); ?>

		<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <div class="medium-3 columns recent-post">
                <a href="<?php the_permalink() ?>">
					<?php the_post_thumbnail('article-thumbnail'); ?>
                </a>
                <h5 class="cat-title">
					<?php
					$url = get_site_url();
					foreach ((get_the_category()) as $cat) {
						if (
							$cat->cat_name !== 'blog')
							echo '<a class="cat-title-link" href="' . $url . '/category/' . $cat->slug . '" title="' . $cat->slug . '">' . $cat->cat_name . '</a> ';
					} ?>
                </h5>
                <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                    <h1 class="recent-post-title"><?php the_title(); ?></h1>
                </a>
            </div>
		<?php endwhile;
		wp_reset_postdata(); ?>
    </div>
</div>