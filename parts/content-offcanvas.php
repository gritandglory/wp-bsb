<?php
/**
 * The template part for displaying offcanvas content
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div class="off-canvas position-right" id="canvas-right" >
	<div class="mobile-menu">
		<?php off_canvas_nav_mobile(); ?>
	</div>
</div>
<div class="off-canvas position-left" id="canvas-left" >
	<div class="mobile-menu">
		<?php off_canvas_nav_desktop(); ?>
	</div>
</div>
<div class="js-off-canvas-overlay"></div>