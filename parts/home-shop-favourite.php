<?php
/**
 * Template part for displaying an Home - 4 columns shop favourites
 */
?>
<div class="home-section">
    <div class="row">
        <h2 class="home-section_title"><?php the_field('shop_favourites_title'); ?></h2>
        <div class="medium-3 columns shop-favourite">
			<?php
			$shop_col_4 = get_field('shop_favourites_group_1');
			$title = $shop_col_4['shop_favourites_title_1'];
			$image = $shop_col_4['shop_favourites_image_1'];
			$url = $shop_col_4['shop_favourites_url_1'];
			$cat = $shop_col_4['shop_favourites_cat_1'];
			if ($shop_col_4): ?>
                <div class="shop-favourite_container">
                    <div class="shop-favourite_img">
                        <a href="<?php echo $url; ?>">
							<?php
							$imageArray = $image;
							$imageAlt = esc_attr($imageArray['alt']);
							$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
							?>
                            <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                        </a>
                    </div>
                    <div class="shop-favourite_content">
                        <h5 class="cat-title">
                            <a class="cat-title-link" href="<?php echo $url; ?>"
                               title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                        </h5>
                        <a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
                            <h2 class="recent-post-title"><?php echo $title; ?></h2>
                        </a>
                    </div>
                </div>
			<?php endif; ?>
        </div>
        <div class="medium-3 columns shop-favourite">
			<?php
			$shop_col_4 = get_field('shop_favourites_group_2');
			$title = $shop_col_4['shop_favourites_title_2'];
			$image = $shop_col_4['shop_favourites_image_2'];
			$url = $shop_col_4['shop_favourites_url_2'];
			$cat = $shop_col_4['shop_favourites_cat_2'];
			if ($shop_col_4): ?>
                <div class="shop-favourite_container">
                    <div class="shop-favourite_img">
                        <a href="<?php echo $url; ?>">
							<?php
							$imageArray = $image;
							$imageAlt = esc_attr($imageArray['alt']);
							$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
							?>
                            <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                        </a>
                    </div>
                    <div class="shop-favourite_content">
                        <h5 class="cat-title">
                            <a class="cat-title-link" href="<?php echo $url; ?>"
                               title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                        </h5>
                        <a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
                            <h2 class="recent-post-title"><?php echo $title; ?></h2>
                        </a>
                    </div>
                </div>
			<?php endif; ?>
        </div>
        <div class="medium-3 columns shop-favourite">
			<?php
			$shop_col_4 = get_field('shop_favourites_group_3');
			$title = $shop_col_4['shop_favourites_title_3'];
			$image = $shop_col_4['shop_favourites_image_3'];
			$url = $shop_col_4['shop_favourites_url_3'];
			$cat = $shop_col_4['shop_favourites_cat_3'];
			if ($shop_col_4): ?>
                <div class="shop-favourite_container">
                    <div class="shop-favourite_img">
                        <a href="<?php echo $url; ?>">
							<?php
							$imageArray = $image;
							$imageAlt = esc_attr($imageArray['alt']);
							$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
							?>
                            <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                        </a>
                    </div>
                    <div class="shop-favourite_content">
                        <h5 class="cat-title">
                            <a class="cat-title-link" href="<?php echo $url; ?>"
                               title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                        </h5>
                        <a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
                            <h2 class="recent-post-title"><?php echo $title; ?></h2>
                        </a>
                    </div>
                </div>
			<?php endif; ?>
        </div>
        <div class="medium-3 columns shop-favourite">
			<?php
			$shop_col_4 = get_field('shop_favourites_group_4');
			$title = $shop_col_4['shop_favourites_title_4'];
			$image = $shop_col_4['shop_favourites_image_4'];
			$url = $shop_col_4['shop_favourites_url_4'];
			$cat = $shop_col_4['shop_favourites_cat_4'];
			if ($shop_col_4): ?>
                <div class="shop-favourite_container">
                    <div class="shop-favourite_img">
                        <a href="<?php echo $url; ?>">
							<?php
							$imageArray = $image;
							$imageAlt = esc_attr($imageArray['alt']);
							$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
							?>
                            <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                        </a>
                    </div>
                    <div class="shop-favourite_content">
                        <h5 class="cat-title">
                            <a class="cat-title-link" href="<?php echo $url; ?>"
                               title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                        </h5>
                        <a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
                            <h2 class="recent-post-title"><?php echo $title; ?></h2>
                        </a>
                    </div>
                </div>
			<?php endif; ?>
        </div>
    </div>
</div>