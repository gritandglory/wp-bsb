<?php
/**
 * Template part for displaying an Home - 3 columns
 */
?>

<div class="fw-row branding">
	<div class="row">
		<div class="branding-banner">
			<div class="medium-2 large-2 columns">
                <img src="https://assets.thebudgetsavvybride.com/wp-content/uploads/2018/08/Glamour-2.png" />
			</div>
			<div class="medium-2 large-2 columns">
                <img src="https://assets.thebudgetsavvybride.com/wp-content/uploads/2018/08/Cosmo.png" />
			</div>
			<div class="medium-2 large-2 columns">
                <img src="https://assets.thebudgetsavvybride.com/wp-content/uploads/2018/08/Bustle-1.png" />
			</div>
			<div class="medium-2 large-2 columns">
                <img src="https://assets.thebudgetsavvybride.com/wp-content/uploads/2018/08/Brides-1.png" />
			</div>
			<div class="medium-2 large-2 columns">
                <img src="https://assets.thebudgetsavvybride.com/wp-content/uploads/2018/08/Huffpost-1.png" />
			</div>
			<div class="medium-2 large-2 columns">
                <img src="https://assets.thebudgetsavvybride.com/wp-content/uploads/2018/08/Refnery29-1.png" />
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>