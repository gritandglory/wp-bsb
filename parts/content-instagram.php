<style type="text/css">
.insta-box,.insta-icon,.item_box{position:absolute}.instagram-container{margin:4rem auto 0}.instagram-container h2{max-width:75.5rem;margin:0 auto;text-align:center}.item_box{height:auto;padding-right:0;padding-left:0;-webkit-transition:opacity .25s;transition:opacity .25s}.item_box:hover .insta-box{opacity:1;-webkit-transition:opacity .25s;transition:opacity .25s}.insta-box{opacity:0;top:0;left:0;background:0 0;width:100%;-webkit-transition:opacity .25s;transition:opacity .25s}.insta-icon{text-align:center;top:40%;color:#f37d90;font-size:32px;width:100%}.photo-thumb{height:270px;width:100%}.instagram-inner{width:100%;display:block;margin:0 auto;position:relative}.instagram-container .slick-slide{max-width:270px;margin:0;position:relative;height:270px;width:100%}

</style>

<div class="instagram-container">


<div class="instagram-inner">



<div class="slider regular">

<?php


// use this instagram access token generator http://instagram.pixelunion.net/
$access_token="560353066.72f4ee7.e1a881a39c5f4b3e86c4a24989c08153";
$photo_count=12;
     
$json_link="https://api.instagram.com/v1/users/self/media/recent/?";
$json_link.="access_token={$access_token}&count={$photo_count}";

$json = file_get_contents($json_link);
$obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);



foreach ($obj['data'] as $post) {
     
    $pic_text=$post['caption']['text'];
    $pic_link=$post['link'];
    $pic_like_count=$post['likes']['count'];
    $pic_comment_count=$post['comments']['count'];
    $pic_src=str_replace("http://", "https://", $post['images']['standard_resolution']['url']);
    $pic_created_time=date("F j, Y", $post['caption']['created_time']);
    $pic_created_time=date("F j, Y", strtotime($pic_created_time . " +1 days"));
     
    echo "<div class='item_box'>";        
        echo "<a href='{$pic_link}' target='_blank'>";
            echo "<img class='img-responsive photo-thumb' src='{$pic_src}' alt='{$pic_text}'>";
            echo "<div class='insta-box'><div class='insta-icon'><i class='fab fa-instagram'></i></div></div>";

        echo "</a>";
    echo "</div>";
}


?>


</div> <!-- end of slider -->
</div>
</div>

  <script src="<?php echo get_template_directory_uri(); ?>/assets/slick/slick.js" type="text/javascript" charset="utf-8"></script>

