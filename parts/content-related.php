<div class="related-post_wrapper">
    <h4><strong>More Posts You’ll Love</strong></h4>
	<?php
	$args = array(
		'posts_per_page' => 3,
		'post__not_in' => array(get_the_ID()),
		'no_found_rows' => true,
	);
	$cats = wp_get_post_terms(get_the_ID(), 'category');
	$cats_ids = array();
	foreach ($cats as $wpex_related_cat) {
		$cats_ids[] = $wpex_related_cat->term_id;
	}
	if (!empty($cats_ids)) {
		$args['category__in'] = $cats_ids;
	}
	$wpex_query = new wp_query($args);
	foreach ($wpex_query->posts as $post) :
	setup_postdata($post); ?>
    <div class="medium-4 columns">
        <div class="related-post_container">
            <div class="related-post_img">
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
					<?php if (has_post_thumbnail()) : ?>
						<?php the_post_thumbnail('article-thumbnail') ?>
					<?php endif ?>
                </a>
            </div>
            <div class="related-post_content">
                <h5 class="cat-title">
					<?php
					$url = get_site_url();
					foreach ((get_the_category()) as $cat) {
						if (
							$cat->cat_name !== 'feature-post')
							echo '<a class="cat-title-link" href="' . $url . '/category/' . $cat->slug . '" title="' . $cat->slug . '">' . $cat->cat_name . '</a> ';
					} ?>
                </h5>
                <h3 class="recent-post-title">
                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                      <?php the_title(); ?>
                    </a>
                </h3>
            </div>
        </div>
    </div>
		<?php endforeach;
		wp_reset_postdata(); ?>
        <div class="clearfix"></div>

</div>

