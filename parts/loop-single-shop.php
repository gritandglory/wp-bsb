<?php 

$cat = get_queried_object();

$args = array(
    'post_type'			=> 'shop_type',
    'posts_per_page'    => -1,
    'tax_query' => array(
        array(
            'taxonomy' => 'custom_cat-shop',
            'field' => 'slug',
            'terms' => $cat->slug, 
        )
    ),
);

$the_query = new WP_Query( $args );

?>

<?php if( $the_query->have_posts() ): ?>
	
<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

<div class="product-<?php the_ID(); ?> product-container">

	<div class="product-image">
		<a href="<?php echo the_permalink(); ?>">
			<?php 

				$image = get_field('product_image');

				if( !empty($image) ): ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			
		</a>
	</div>
	
	<div class="product-content">
		<h2><?php the_field('product_name'); ?></h2>
		<p><?php the_field('shop_name'); ?> </p>
		<p>$ <?php the_field('product_price'); ?> </p>
		<a id="shop-link" class="btn btn-coral" href="<?php  the_permalink(); ?>" target="_blank" >Shop Now</a>
			
	</div>
</div>

	<?php endwhile; ?>

<?php endif; ?>

<?php wp_reset_query();	 ?>