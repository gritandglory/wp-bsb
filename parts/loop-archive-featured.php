<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>
<div class="featured-container">
<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">		

	
	
		<div class="entry-thumbnail">
			<a href="<?php the_permalink() ?>"><?php the_post_thumbnail('article-thumbnail_x2'); ?></a>
			<span class="the-deal">
				<h2>Featured</h2>

			</span>
		</div>	
	
		<div class="entry-content">
			<h2 class="archive-title">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"> <?php the_title();?>
					
				</a>
			</h2>
			<div class="entry-excerpt">
				<?php the_excerpt(30, 'content'); ?>
			</div>
			
			<div class="entry-footer">
				<div class="post-author">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/Circle Image (author).jpg"> <?php the_author_posts_link(); ?>
				</div>
			
			<div class="btn btn-tq"> Read More <i class="fas fa-angle-right"></i></div>

			</div>
		</div>

	
    						
</article>

</div>