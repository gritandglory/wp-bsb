<?php
/**
 * Template part: Latest post for home
 */
?>
<div class="home-section">
    <div class="row">
        <h2 class="home-section_title">Top Money-Saving Hacks</h2>
        <div class="large-9 columns extra_padding">
	        <?php $counter = 1;?>
			<?php $the_query = new WP_Query(
				array('category_name' => 'money-saving-advice',
					'posts_per_page' => 4)
			); ?>

			<?php

            while ($the_query->have_posts()) :
			$the_query->the_post();

			?>
            <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope
                     itemtype="http://schema.org/Blog">
                <div class="archive-entry-container"  <?php if ($counter === 4) { echo 'style="margin:0;"';} ?>>
                    <div class="archive-entry-thumbnail">
                        <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('article-thumbnail'); ?></a>
                    </div>
                    <div class="archive-entry-content">
                        <h5 class="cat-title">
							<?php
							$categories = get_the_category();
							$separator = " • ";
							$output = '';
							if ($categories) {
								foreach ($categories as $category) {
									if ($category->cat_name != 'blog')
										if ($category->cat_name != 'Featured Post') {
											$output .= '<a class="cat-title-link"  href="' . get_category_link($category->term_id) . '" title="' . esc_attr(sprintf(__("View all posts in %s"), $category->name)) . '">' . $category->cat_name . '</a>' . ' <span>' . $separator . '</span>';
										}
								}
								echo trim($output, $separator);
							}
							?>
                        </h5>
                        <a href="<?php the_permalink() ?>" rel="bookmark"
                           title="<?php the_title_attribute(); ?>">
                            <h2 class="recent-post-title" itemprop="headline"><?php the_title(); ?></h2>
                        </a>
                        <div class="entry-excerpt"><p>
		                        <?php miniExcerpt(300); ?>
                            </p>

                        </div>
                        <div class="article-details">
                            <div class="post-author">
                                <p rel="author" class="author-name">By <?php the_author_posts_link(); ?></p>
                            </div>
                            <a class="read-more" href="<?php the_permalink() ?>">Read Post </a>
                        </div>
                    </div>
            </article>
	            <?php $counter++ ;?>
				<?php endwhile;
				wp_reset_postdata(); ?>

        </div>

		<?php if (is_active_sidebar('sidebar_home')) : ?>
        <div id="sidebar_hacks" class="sidebar_hacks small-12 large-3 column" role="complementary">

	        <?php dynamic_sidebar('sidebar_home'); ?>
        </div>

		<?php endif; ?>
    </div>
</div>