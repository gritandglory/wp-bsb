<?php
/**
 * Template part for displaying a single post
 */
?>
<div class="post-details">
    <div class="side-fixed">
        <div class="author-image">
			<?php echo get_avatar(get_the_author_meta('ID'), 150); ?>
        </div>
        <div class="post-social">
            <div class="post--share">
                <ul>
                    <li><a class="subscribe" href="#"><h4>subscribe</h4><i class="far fa-envelope"></i></a></li>
                    <li><a class="share-header"><h4>share</h4><i class="fas fa-share"></i></a></li>
                    <li><a href="https://m.me/budgetsavvybride" target="_blank"><h4>chat</h4><i
                                    class="fab fa-facebook-messenger"></i></a></li>
                </ul>
            </div>
            <div class="post--social">
				<?php
				$postURL = urlencode(get_permalink());
				$postTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
				$postThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				$twitterURL = 'https://twitter.com/intent/tweet?text=' . $postTitle . '&amp;url=' . $postURL;
				$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $postURL;
				$pinterestURL = 'https://pinterest.com/pin/create/button/?url=' . $postURL . '&amp;media=' . $postThumbnail[0] . '&amp;description=' . $postTitle;
				$whatsappURL = 'whatsapp://send?text=' . $postURL;
				?>
                <ul id="sls-buttons">
                    <li class="facebook"><a target="_blank" href="<?php echo $facebookURL ?>" class="facebook"><h4>
                                Facebook</h4><i class="fab fa-facebook"></i></a></li>
                    <li class="twitter"><a target="_blank" href="<?php echo $twitterURL ?>" class="twitter"><h4>
                                Twitter</h4><i class="fab fa-twitter"></i></a></li>
                    <li class="pinterest"><a target="_blank" href="<?php echo $pinterestURL ?>" class="pinterest"><h4>
                                Pinterest</h4><i class="fab fa-pinterest"></i></a></li>
                    <li class="email"><a target="_top"
                                         href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo $postURL ?>"
                                         class="email"><h4>Email</h4><i class="fa fa-envelope"></i></a></li>
                    <li class="pinterest"><a href="<?php echo $whatsappURL ?>" data-action="share/whatsapp/share"><h4>
                                Whatsapp</h4><i class="fab fa-whatsapp"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="article-content">
    <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
        <header class="article-header">
            <h1 class="entry-title single-title"><?php the_title(); ?></h1>
            <div class="tags" style="text-align: left; margin: 1rem 0;">
				<?php
				$tags = get_the_tags();
				$separator = ' ';
				$output = '';
				if (!empty($tags)) {
					foreach ($tags as $tag) {
						$output .= '<a href="' . esc_url(get_tag_link($tag->term_id)) . '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $tag->name)) . '" style="color: #F37D90">' . '#' . esc_html($tag->name) . '</a>' . $separator;
					}
					echo trim($output, $separator);
				}
				?>
            </div>
        </header>
        <section class="entry-content blog_content">
            <p class="link-disclaimer">This post may contain affiliate links. Click <a
                        href="<?php echo get_site_url(); ?>/about/policies">here</a> to learn more.</p>
			<?php if (have_rows('list_repeater')): ?>
                <ul class="bullet-list">
					<?php while (have_rows('list_repeater')) : the_row();
						$item = get_sub_field('list_item');
						?>
                        <li><?php echo $item ?></li>
					<?php endwhile; ?>
                </ul>
			<?php endif; ?>

			<?php the_content(); ?>
        </section>
        <footer class="article-footer">
			<?php get_template_part('parts/content', 'related'); ?>
            <div class="clearfix"></div>
            <div id="amzn-assoc-ad-883ba310-72c8-497a-a930-5d72d46f3df7"></div>
            <script async
                    src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=883ba310-72c8-497a-a930-5d72d46f3df7"></script>
			<?php get_template_part('parts/content', 'author'); ?>
        </footer>
		<?php comments_template(); ?>
    </article>
</div>