<?php 

$terms = get_the_terms( $post->ID , 'custom_cat-shop', 'string');

$term_ids = wp_list_pluck($terms,'term_id');

$second_query = new WP_Query( array(
	'post_type' => 'shop_type', 
	'tax_query' => array(
		array(
			'taxonomy' => 'custom_cat-shop',
			'field' => 'id',
			'terms' => $term_ids,
			'operator'=> 'IN'
		)),
	'posts_per_page' => 4,
	'ignore_sticky_posts' => 1,
	'orderby' => 'rand',
	'post__not_in'=>array($post->ID)
) ); ?>

<?php if($second_query->have_posts()): ?> 

	<div class="related-shop">

		<h2 class="another-title">Related Products</h2>
		
		<?php while ($second_query->have_posts() ) : $second_query->the_post(); ?>

			<div class="related--shop medium-3 columns">

				<div class="product-image">

					<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
						
						<?php 

						$image = get_field('product_image');

						if( !empty($image) ): ?>

							<img class="product--image" src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

						<?php endif; ?>
						
						<div class="overlay">

							<div class="text">View</div>

						</div>

					</a>
				</div>

				<h4 class="product--name"><?php the_field('product_name'); ?></h4>
				
				<p class="shop--name"><?php the_field('shop_name'); ?> </p>
				
				<p class="product--price">$ <?php the_field('product_price'); ?> </p>

			</a>

		</div>

	<?php endwhile; wp_reset_query();?>

<?php endif; ?>

<div class="clearfix"></div>
</div>