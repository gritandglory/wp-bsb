<?php
/**
 * Template part: Latest post for home
 */
?>
<div class="home-section first">
    <div class="row">
        <h2 class="home-section_title ">Latest Posts</h2>
        <div class="large-6 columns ">
			<?php
			global $post;
			$args = array('posts_per_page' => 1, 'offset' => 0,);
			$myposts = get_posts($args);
			foreach ($myposts as $post) : setup_postdata($post); ?>
                <div class="home_most_recent-post">
                    <div class="home_most_recent-post_img">
                        <a href="<?php the_permalink() ?>">
							<?php the_post_thumbnail('article-thumbnail'); ?>
                        </a>
                    </div>
                    <div class="home_most_recent-post_content">
                        <h5 class="cat-title">
							<?php
							$url = get_site_url();
							foreach ((get_the_category()) as $cat) {
								if (
									$cat->cat_name !== 'blog')
									echo '<a class="cat-title-link" href="' . $url . '/category/' . $cat->slug . '" title="' . $cat->slug . '">' . $cat->cat_name . '</a> ';
							} ?>
                        </h5>
                        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                            <h2 class="recent-post-title"><?php the_title(); ?></h2>
                        </a>
                        <p><?php miniExcerpt(400) ?></p>
                        <a class="read-more" href="<?php the_permalink() ?>">Read Post </a>
                    </div>
                </div>
			<?php endforeach;
			wp_reset_postdata(); ?>
        </div>
        <div class="large-6 columns ">
			<?php
			global $post;
			$args = array('posts_per_page' => 5, 'offset' => 1,);
			$myposts = get_posts($args);
			foreach ($myposts as $post) : setup_postdata($post); ?>
                <div class="home_latest-post">
                    <div class="home_latest-post_img">
                        <a href="<?php the_permalink() ?>">
							<?php the_post_thumbnail('article-thumbnail'); ?>
                        </a>
                    </div>
                    <div class="home_latest-post_content">
                        <h5 class="cat-title">
							<?php
							$url = get_site_url();
							foreach ((get_the_category()) as $cat) {
								if (
									$cat->cat_name !== 'blog')
									echo '<a class="cat-title-link" href="' . $url . '/category/' . $cat->slug . '" title="' . $cat->slug . '">' . $cat->cat_name . '</a> ';
							} ?>
                        </h5>
                        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>">
                            <h2 class="recent-post-title"><?php the_title(); ?></h2>
                        </a>
                        <p>
							<?php
							miniExcerpt(96) ?></p>
                        <a class="read-more" href="<?php the_permalink() ?>">Read Post </a>
                    </div>
                </div>
			<?php endforeach;
			wp_reset_postdata(); ?>
        </div>
    </div>
</div>