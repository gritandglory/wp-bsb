<?php
/**
 * Template part for displaying an Home - 3 columns
 */
?>
<div class="home-section">
    <div class="row">
        <h2 class="home-section_title"><?php the_field('popular_post_title'); ?></h2>
        <div class=" medium-3 columns">
			<?php
			$post = get_field('popular_post_1_group');
			$title = $post['popular_post_title_1'];
			$url = $post['popular_post_url_1'];
			$cat = $post['popular_post_cat_1'];
			if ($post): ?>
                <div class="popular_post_container">

						<?php
						$imageArray = $post['popular_post_image_1'];
						$imageAlt = esc_attr($imageArray['alt']);
						$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
						?>
                        <div class="popular_post_img">
                            <a href="<?php echo $url; ?>">
                            <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                            </a>
                        </div>
                        <div class="popular_post_content">
                            <h5 class="cat-title">
                                <a class="cat-title-link" href="<?php echo $url; ?>"
                                   title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                            </h5>
                            <h2 class="recent-post-title"><?php echo $title; ?></h2>
                        </div>

                </div>
			<?php endif; ?>
        </div>
        <div class=" medium-3 columns">
			<?php
			$post = get_field('popular_post_2_group');
			$title = $post['popular_post_title_2'];
			$url = $post['popular_post_url_2'];
			$cat = $post['popular_post_cat_2'];
			if ($post): ?>
                <div class="popular_post_container">

						<?php
						$imageArray = $post['popular_post_image_2'];
						$imageAlt = esc_attr($imageArray['alt']);
						$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
						?>
                        <div class="popular_post_img">
                            <a href="<?php echo $url; ?>">
                            <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                            </a>
                        </div>
                        <div class="popular_post_content">
                            <h5 class="cat-title">
                                <a class="cat-title-link" href="<?php echo $url; ?>"
                                   title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                            </h5>
                            <h2 class="recent-post-title"><?php echo $title; ?></h2>
                        </div>

                </div>
			<?php endif; ?>
        </div>
        <div class=" medium-3 columns">
			<?php
			$post = get_field('popular_post_3_group');
			$title = $post['popular_post_title_3'];
			$url = $post['popular_post_url_3'];
			$cat = $post['popular_post_cat_3'];
			if ($post): ?>
            <div class="popular_post_container">

					<?php
					$imageArray = $post['popular_post_image_3'];
					$imageAlt = esc_attr($imageArray['alt']);
					$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
					?>
                    <div class="popular_post_img">
                        <a href="<?php echo $url; ?>">
                        <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                        </a>
                    </div>
                    <div class="popular_post_content">
                        <h5 class="cat-title">
                            <a class="cat-title-link" href="<?php echo $url; ?>"
                               title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                        </h5>
                        <h2 class="recent-post-title"><?php echo $title; ?></h2>
                    </div>

            </div>
			<?php endif; ?>
        </div>

    <div class=" medium-3 columns">
		<?php
		$post = get_field('popular_post_4_group');
		$title = $post['popular_post_title_4'];
		$url = $post['popular_post_url_4'];
		$cat = $post['popular_post_cat_4'];
		if ($post): ?>
        <div class="popular_post_container">

				<?php
				$imageArray = $post['popular_post_image_4'];
				$imageAlt = esc_attr($imageArray['alt']);
				$imageThumbURL = esc_url($imageArray['sizes']['article-thumbnail']);
				?>
                <div class="popular_post_img">
                    <a href="<?php echo $url; ?>">
                    <img src="<?php echo $imageThumbURL; ?>" alt="<?php echo $imageAlt; ?>">
                    </a>
                </div>
                <div class="popular_post_content">
                    <h5 class="cat-title">
                        <a class="cat-title-link" href="<?php echo $url; ?>"
                           title="<?php echo $title; ?>"><?php echo $cat; ?></a>
                    </h5>
                    <h2 class="recent-post-title"><?php echo $title; ?></h2>
                </div>

        </div>
		<?php endif; ?>
    </div>
</div>

</div>
