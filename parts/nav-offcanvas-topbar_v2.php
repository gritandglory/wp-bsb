<?php
?>
<div class="has-header ">
    <ul class="desktop-icon">
        <li>
            <button id="hamburger-desk" class="button hamburger hamburger--spin" type="button"
                    data-toggle="off-canvas-left">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
            </button>
        </li>
    </ul>
    <a class="logo-header" href="<?php echo home_url(); ?>">
        <div class="logo-desktop">
			<?php get_template_part('parts/content', 'logo-desktop'); ?>
        </div>
    </a>
    <nav class="desktop-nav" role="navigation">
		<?php main_top_nav(); ?>
    </nav>
    <nav class="desktop-search" role="navigation">
        <ul id="menu-top" class="medium-horizontal menu" data-responsive-menu="accordion medium-dropdown">
            <li id="menu-item-86691"
                class="search-btn menu-item menu-item-type-custom menu-item-object-custom menu-item-86691"><a><i
                            class="fas fa-search"></i></a></li>
        </ul>
    </nav>
</div>
<div class="has-header-scroll">
    <ul>
        <li><a class="subscribe" href="#"><h4>subscribe</h4><i class="far fa-envelope"></i></a></li>
        <li><a class="share-header"><h4>share</h4><i class="fas fa-share"></i></a></li>
        <li><a href="https://m.me/budgetsavvybride" target="_blank"><h4>chat</h4><i
                        class="fab fa-facebook-messenger"></i></a></li>
    </ul>
</div>
<div class="has-header-share">


	<?php
	$postURL = urlencode(get_permalink());
	$postTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
	$postThumbnail = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
	$twitterURL = 'https://twitter.com/intent/tweet?text=' . $postTitle .' via @savvybride ' . '&amp;url=' . $postURL;
	$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u=' . $postURL;
	$pinterestURL = 'https://pinterest.com/pin/create/button/?url=' . $postURL . '&amp;media=' . $postThumbnail[0] . '&amp;description=' . $postTitle;
	$whatsappURL = 'whatsapp://send?text=' . $postURL;
	?>
    <ul id="sls-buttons">
        <li class="facebook"><a target="_blank" href="<?php echo $facebookURL ?>" class="facebook"><h4>Facebook</h4><i
                        class="fab fa-facebook"></i></a></li>
        <li class="twitter"><a target="_blank" href="<?php echo $twitterURL ?>" class="twitter"><h4>Twitter</h4><i
                        class="fab fa-twitter"></i></a></li>
        <li class="pinterest"><a target="_blank" href="<?php echo $pinterestURL ?>" class="pinterest"><h4>Pinterest</h4>
                <i class="fab fa-pinterest"></i></a></li>
        <li class="email"><a target="_top"
                             href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site <?php echo $postURL ?>"
                             class="email"><h4>Email</h4><i class="fa fa-envelope"></i></a></li>
        <li class="pinterest"><a href="<?php echo $whatsappURL ?>" data-action="share/whatsapp/share"><h4>Whatsapp</h4>
                <i class="fab fa-whatsapp"></i></a></li>
    </ul>
</div>











