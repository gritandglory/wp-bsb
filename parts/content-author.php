<?php
/**
 * Template part for displaying an Author
 */
?>

<div class="author-container">
	<div class="author-image">

		<?php echo get_avatar( get_the_author_meta('ID'), 150); ?>
		
	</div>

	<div class="author-content">

		<p class="author-description"><span itemprop="author" itemscope itemtype="http://schema.org/Person"><?php the_author_posts_link(); ?></span> <?php the_author_meta('description');?></p>
		
	</div>
	<div class="author-sl">
			<h4>Connect with <?php the_author_meta('first_name');?>:</h4>
			<?php  
			if(function_exists('user_social_links')) :
				$atts = array('username'  => get_the_author_meta('user_login'),);
				echo user_social_links($atts);
			endif;
			?>
		</div>

</div>


 
