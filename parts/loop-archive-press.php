<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>

<div class="press-<?php the_ID(); ?> press-container">

	<div class="press-image">
		<?php 

		$image = get_field('press_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>
	</div>
	<div class="press-content">
		<h4><a href="<?php echo the_field('press_image_url'); ?>"><?php the_field('press_link_text'); ?></a></h4>
	
		<h4><?php the_field('press_date'); ?></h4>

	</div>
		
</div>

<hr>
