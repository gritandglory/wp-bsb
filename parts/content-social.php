<?php
/**
* Template part for displaying a single post
*/
?>

<?php

$postURL = urlencode(get_permalink());

$postTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
$postThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
$twitterURL = 'https://twitter.com/intent/tweet?text='.$postTitle.'&amp;url='.$postURL;
$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$postURL;
$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$postURL.'&amp;media='.$postThumbnail[0].'&amp;description='.$postTitle;

?>

<ul id="sls-buttons">

    <li><a target="_blank" href="<?php echo $facebookURL ?>" class="facebook"><i class="fab fa-facebook"></i></a></li>

    <li><a target="_blank" href="<?php echo $twitterURL ?>" class="twitter"><i class="fab fa-twitter"></i></a></li>

    <li><a target="_blank" href="<?php echo $pinterestURL ?>" class="pinterest"><i class="fab fa-pinterest"></i></a></li>

</ul>

