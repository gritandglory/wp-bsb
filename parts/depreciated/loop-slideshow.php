<?php
/**
 * Template part for displaying a single post
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

    <header class="article-header"> 
        <div class="post-author">
            <p rel="author" class="author-name" >By <?php the_author_posts_link(); ?></p>
            
        </div>
        <h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
        <div class="tags" style="text-align: center; margin: 1rem 0;">
        <?php
            $tags = get_the_tags();
            $separator = ' ';
            $output = '';
            if ( ! empty( $tags ) ) {
            foreach( $tags as $tag ) {
                $output .= '<a href="' . esc_url( get_tag_link( $tag->term_id ) ) . '" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $tag->name ) ) . '" style="color: #F37D90">'.'#' . esc_html( $tag->name ) . '</a>' . $separator;
            }
            echo trim( $output, $separator );
            }
        ?>
        </div>

        <div class="post-ss">
            <ul>
                <li><h4>Share this post</h4></li>

                <?php get_template_part( 'parts/content', 'social' ); ?>
    
            </ul>

        </div>      

    </header> 

    <section class="entry-content" itemprop="articleBody">
        <p class="link-disclaimer">This post may contain affiliate links. Click <a href="<?php echo get_site_url(); ?>/about/policies">here</a>  to learn more.</p> 
         
         <?php if( have_rows('slide_repeater') ): ?>

                <div id="slideshow" class="slideshow-container">

                    <?php while( have_rows('slide_repeater') ): the_row(); 

                        $image          = get_sub_field('image');
                        $title          = get_sub_field('title');
                        $text           = get_sub_field('content');

                        ?>

                        <div class="slide-container" >

                            <div class="post">

                                <?php if( !empty($image) ): ?>

                                    <img src="<?php echo $image['sizes']['article-thumbnail_x2']; ?>" alt="<?php echo $image['alt']; ?>" />

                                <?php endif; ?>

                                <?php if( !empty($title) ): ?>

                                    <h2>

                                        <?php echo $title; ?>

                                    </h2>

                                <?php endif; ?>

                                <?php if( !empty($text) ): ?>

                                    <?php echo $text; ?>

                                <?php endif; ?>
                           
                                </div>

                            </div>


                        <?php endwhile; ?>

                    </div>

                <?php endif; ?>
    </section> 

    <footer class="article-footer">

        <?php get_template_part( 'parts/content', 'related' ); ?>
        <div class="clearfix"></div>

        <div id="amzn-assoc-ad-883ba310-72c8-497a-a930-5d72d46f3df7"></div><script async src="//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=883ba310-72c8-497a-a930-5d72d46f3df7"></script>

        <?php get_template_part( 'parts/content', 'author' ); ?>



    </footer> 

    <?php comments_template(); ?>   


</article> 



           
