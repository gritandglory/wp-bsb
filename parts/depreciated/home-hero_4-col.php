<?php
/**
 * Template part for displaying an Home - Hero section top 3 columns
 */
?>


<style type="text/css">
	.featured-container {
		margin: 3rem auto;
	}


</style>

<div class="row featured-container">

	<div class="medium-4 columns featured-thumbnail">
	
		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>



		<span class="feature-label">

			<h2>Featured</h2>

		</span>

		<div class="below-text">
			<h4><?php the_field('hero_title'); ?></h4>
		</div>

	</div>

	<div class="medium-4 columns featured-thumbnail">
	
		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>

		<span class="feature-label">

			<h2>Featured</h2>

		</span>

		<div class="below-text">

			<h4><?php the_field('hero_title'); ?></h4>

		</div>

	</div>

	<div class="medium-4 columns featured-thumbnail">

		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>

		<span class="feature-label">

			<h2>Featured</h2>

		</span>

		<div class="below-text">

			<h4><?php the_field('hero_title'); ?></h4>

		</div>

	</div>

</div>
