<?php

?>

<div class="top-main-scroll">
	<div class="row">
		<nav>
			<a class="logo-header" href="<?php echo home_url(); ?>">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/BSB-logo.svg">
			</a>
			<div class="scroll-menu-container">
				<?php main_top_nav_scroll(); ?>
			</div>
		</nav>
	</div>
</div>


<div class="upper-top-bar">
	<div class="row">
		<div class="upper-top-left">
			<nav>
				<?php main_top_left_nav(); ?>
			</nav>
			
		</div>
		<div class="upper-top-center">
			<?php if ( is_active_sidebar( 'sidebar2' ) ) : ?>

				<?php dynamic_sidebar( 'sidebar2' ); ?>

				<?php else : ?>
			<?php endif; ?>
		</div>
		<div class="upper-top-right">
			<nav>
				<?php main_top_right_nav(); ?>
			</nav>

			
		</div>
	</div>
</div>
<div class="logo-slogan">
	<div class="row">
		<a class="logo-header" href="<?php echo home_url(); ?>">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/BSB-logo.svg">
		</a>
		<div class="slogan">
			<h2>helping couples create beautiful weddings without breaking the bank</h2>
		</div>
	</div>
</div>

<div class="top-main-bar">
	<div class="row">
		<div class="top-menu-container">
			<nav role="navigation">
				<?php main_top_nav(); ?>
			</nav>
		</div>
	</div>
</div>
