<?php
/**
 * Template part for displaying an Home - Hero section slider
 */
?>
<style type="text/css">
    .hero-inner {
        max-height: 100%;
        width: 100%;
    }

    .hero-inner .slide img {
        display: block;
        width: 100%;
        height: auto;
    }
</style>
<div class="hero-container">
	<?php if (have_rows('hero_slider_repeater')): ?>
        <div class="hero-inner row expanded">
            <div id="hero_slider" class="slider-hero">
				<?php while (have_rows('hero_slider_repeater')): the_row();
					$image = get_sub_field('hero_image');
					$sub_title = get_sub_field('sub_title');
					$title = get_sub_field('hero_title');
					$text = get_sub_field('hero_text');
					$button_text = get_sub_field('hero_button_text');
					$link = get_sub_field('hero_button_link');
					$color = get_sub_field('hero_background_color');
					?>
                    <div class="slider-container large-4 columns">
                        <div class="post">
                            <a href="<?php echo $link; ?>">
								<?php if (!empty($image)): ?>
                                    <img src="<?php echo $image['sizes']['article-thumbnail']; ?>"
                                         alt="<?php echo $image['alt']; ?>"/>
								<?php endif; ?>

								<?php if (!empty($sub_title)): ?>
                                    <h5 class="cat-title">
                                        <a class="cat-title-link" href="<?php echo $link; ?>"
                                           title="<?php echo $image['alt']; ?>"><?php echo $sub_title; ?></a>
                                    </h5>
								<?php endif; ?>
                                <a href="<?php echo $link; ?>" title="<?php echo $title; ?>">
									<?php if (!empty($title)): ?>
                                        <h1 class="recent-post-title"><?php echo $title; ?></h1>
									<?php endif; ?>
                                </a>
                        </div>
                    </div>
				<?php endwhile; ?>
            </div>
        </div>
	<?php endif; ?>
</div>



