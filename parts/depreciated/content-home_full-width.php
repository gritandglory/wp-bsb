<?php
/**
 * Template part for displaying an Home - Hero section full width (row)
 */
?>


<style type="text/css">
	.featured-container-full {
		margin: 0rem auto;
		width: 120vw;
		display: flex;
		margin-left: -10vw;


	}

	.columns.featured-thumbnail {
		padding-left: 8px;
		padding-right: 8px;
	}


</style>

<div class="featured-container-full">

	<div class="large-4 columns featured-thumbnail">
	
		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>





		<div class="below-text">
			<h4><?php the_field('hero_title'); ?></h4>
		</div>

	</div>

	<div class="large-4 columns featured-thumbnail">
	
		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>


		<div class="below-text">

			<h4><?php the_field('hero_title'); ?></h4>

		</div>

	</div>

	<div class="large-4 columns featured-thumbnail">

		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>


		<div class="below-text">

			<h4><?php the_field('hero_title'); ?></h4>

		</div>

	</div>

	<div class="large-4 columns featured-thumbnail">

		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>


		<div class="below-text">

			<h4><?php the_field('hero_title'); ?></h4>

		</div>

	</div>

	<div class="large-4 columns featured-thumbnail">

		<?php 

		$image = get_field('hero_image');

		if( !empty($image) ): ?>

			<img src="<?php echo $image['sizes']['article-thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />

		<?php endif; ?>

	

		<div class="below-text">

			<h4><?php the_field('hero_title'); ?></h4>

		</div>

	</div>

</div>
