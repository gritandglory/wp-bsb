<?php
/**
 * Template part for displaying posts
 *
 * Used for single, index, archive, search.
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope
         itemtype="http://schema.org/Blog">
    <div class="archive-entry-container">
        <div class="archive-entry-thumbnail">
            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('article-thumbnail'); ?></a>
        </div>
        <div class="archive-entry-content">
            <h5 class="cat-title">
				<?php
				$categories = get_the_category();
				$separator = " • ";
				$output = '';
				if ($categories) {
					foreach ($categories as $category) {
						if ($category->cat_name != 'blog')
							if ($category->cat_name != 'Featured Post') {
								$output .= '<a class="cat-title-link"  href="' . get_category_link($category->term_id) . '" title="' . esc_attr(sprintf(__("View all posts in %s"), $category->name)) . '">' . $category->cat_name . '</a>' . ' <span>' . $separator . '</span>';
							}
					}
					echo trim($output, $separator);
				}
				?>
            </h5>
            <h2 class="archive-title" itemprop="headline">
                <a href="<?php the_permalink() ?>" rel="bookmark"
                   title="<?php the_title_attribute(); ?>"> <?php the_title(); ?></a>
            </h2>
            <div class="entry-excerpt">
				<?php the_excerpt(20,'content'); ?>
            </div>
            <div class="article-details">
                <div class="post-author">
                    <p rel="author" class="author-name">By <?php the_author_posts_link(); ?></p>
                </div>
                <a class="read-more" href="<?php the_permalink() ?>">Read Post </a>
            </div>
        </div>
    </div>
</article>