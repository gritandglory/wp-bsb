<?php
$related = new WP_Query(
	array(
		'category__in' => wp_get_post_categories($post->ID),
		'posts_per_page' => 4,
		'post_type' => 'offer_type',
		'post__not_in' => array($post->ID))
);
?>
<?php if ($related->have_posts()): ?>
<div class="related-offer large-12 columns">
    <h2 class="another-title">Current Wedding Deals</h2>
	<?php while ($related->have_posts()) : $related->the_post(); ?>
        <div class="single-deal-related medium-3 columns">
            <div class="single-deal-related_img">
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
					<?php
					$image = get_field('offer_image');
					if (!empty($image)): ?>
                        <img src="<?php echo $image['sizes']['article-thumbnail']; ?>"
                             alt="<?php echo $image['alt']; ?>"/>
					<?php endif; ?>
                    <div class="overlay">
                        <div class="text">View</div>
                    </div>
                </a>
            </div>
            <div class="single-deal-related_content">
                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                <h5 class="single-deal-related_type"><?php the_field('offer_type'); ?></h5>
                <h4 class="single-deal-related_title"><?php the_field('offer_title'); ?></h4>
                <p class="single-deal-related_spec">Sitewide discount. Limited time!</p>
                <h3 class="single-deal-related_offer"><?php the_field('the_offer'); ?> </h3>
                </a>
            </div>
        </div>
	<?php endwhile; wp_reset_query(); ?>

	<?php endif; ?>
    <div class="clearfix"></div>
</div>