<?php
/**
 * The template for displaying all single posts and attachments
 */
get_header(); ?>
    <div class="content-wrapper">
    <div class="row">
        <main id="single" class="main large-9 columns" role="main">
            <div class="inner-content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php get_template_part('parts/loop', 'single'); ?>
				<?php endwhile; else : ?>
					<?php get_template_part('parts/content', 'missing'); ?>
				<?php endif; ?>
            </div>
        </main>
		<?php get_sidebar(); ?>
    </div>
    </div>
<?php get_footer(); ?>