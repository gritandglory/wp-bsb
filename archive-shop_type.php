<?php
/**
 * Displays archive pages if a speicifc template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>
<div class="shop-title-container">
    <h1 class="shop-title">The Budget Savvy Bride Shop</h1>
</div>
<div class="content-wrapper">
    <div class="row">
        <main id="shop" class="main large-12 columns" role="main">
            <div class="archive-inner-content">
				<?php
				$args = array(
					'type' => 'shop_type',
					'taxonomy' => 'custom_cat-shop',
					'order' => 'ASC',
					'hide_empty' => '1');
				$categories = get_categories($args);
				if ($categories) {
					foreach ($categories as $category) {
						$link = get_term_link($category);
						$cat_link = $category->slug;
						$image = get_field('post_category_image', 'category_' . $category->term_id);
						echo '<div class="medium-6 large-4 columns">';
						echo '<div class="cat-container">';
						echo '<a class="cat--link" href="' . $link . '?fwp_categories=' . $cat_link . '">' . '<img src="' . $image['sizes']['article-thumbnail'] . '" />' . '<h2 class="cat-title">' . $category->name . '</h2>' . '<span class="mb-cat-title">' . '<h2>' . $category->name . '</h2>' . '</span>';
						echo '</a>';
						echo '</div>';
						echo '</div>';
					}
				}
				?>
            </div>
        </main>
    </div>
</div>
<?php get_footer(); ?>
