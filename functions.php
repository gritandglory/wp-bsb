<?php
/**
 * For more info: https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

// Theme support options
require_once(get_template_directory().'/functions/theme-support.php');

// Theme support options
require_once(get_template_directory().'/functions/gutenberg.php');


// Theme support options
require_once(get_template_directory().'/functions/embed.php');

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory().'/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory().'/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory().'/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory().'/functions/translation/translation.php');

// Adds site styles to the WordPress editor
require_once(get_template_directory().'/functions/editor-styles.php');

// Remove Emoji Support
// require_once(get_template_directory().'/functions/disable-emoji.php');

// Related post function - no need to rely on plugins
require_once(get_template_directory().'/functions/related-posts.php');


// Use this as a template for custom post types
require_once(get_template_directory().'/functions/post-type-printable.php');

// Use this as a template for custom post types
require_once(get_template_directory().'/functions/post-type-press.php');

// Use this as a template for custom post types
require_once(get_template_directory().'/functions/post-type-offer.php');

// Use this as a template for custom post types
require_once(get_template_directory().'/functions/post-type-shop.php');

// Use this as a template for custom post types
//require_once(get_template_directory().'/functions/post-type-slideshow.php');

// Use this as a template for custom post types
require_once(get_template_directory().'/functions/yoast-primary-cat.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/functions/login.php');

// Customize the WordPress admin
// require_once(get_template_directory().'/functions/admin.php');

// Customize the WordPress admin
require_once(get_template_directory().'/functions/user-social.php');

// Customize the WordPress admin
require_once(get_template_directory().'/functions/shortcode.php');



/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');



function acf_set_featured_image( $value, $post_id, $field  ){

    if($value != ''){
	    //Add the value which is the image ID to the _thumbnail_id meta data for the current post
	    add_post_meta($post_id, '_thumbnail_id', $value);
    }

    return $value;
}

// acf/update_value/name={$field_name} - filter for a specific field based on it's name
add_filter('acf/update_value/name=product_image', 'acf_set_featured_image', 10, 3);

function acf_update_post_thumbnail( $post_id ) {
    $thumbnail_id = get_post_meta( $post_id, 'product_image' );
    $thumbnail_id = get_post_meta( $post_id, 'offer_image' );
    set_post_thumbnail( $post_id, $thumbnail_id );
}
// run after ACF saves the $_POST['acf'] data
add_action( 'acf/save_post', 'acf_update_post_thumbnail', 20 );

function mdc_target_blank($content) {
    $post_string = $content;
    $post_string = str_replace('<a', '<a target="_blank"', $post_string);
    return $post_string;
}

add_filter( 'the_content', 'mdc_target_blank' );


// Add Affilate Link disclaimer to the RSS feed

function agentwp_insertRss($content) {

if(is_feed()){

$url = get_site_url();

$content = '<p class="link-disclaimer">This post may contain affiliate links. Click <a href="'.$url.'/about/policies">here</a>  to learn more.</p> '.$content.'text after content';
}
return $content;
}

add_filter('the_excerpt_rss', 'agentwp_insertRss');




@ini_set( 'upload_max_size' , '128M' );
@ini_set( 'post_max_size', '128M');
@ini_set( 'max_execution_time', '300' );

