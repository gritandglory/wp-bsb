<?php
/*
Template Name: Home Page
*/
get_header(); ?>
<div class="content-wrapper">
    <main id="home-page" class="main" role="main">
		<?php get_template_part('parts/home', 'latest-posts'); ?>
        <div class="clearfix"></div>
		<?php get_template_part('parts/home', '4-column-custom'); ?>
        <div class="clearfix"></div>
		<?php get_template_part('parts/home', 'saving_hacks'); ?>
        <div class="clearfix"></div>
		<?php get_template_part('parts/home', 'shop-favourite'); ?>
        <div class="clearfix"></div>

        <div class="clearfix"></div>
    </main>
    <div class="clearfix"></div>
</div>
<?php get_footer(); ?>
