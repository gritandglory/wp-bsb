<?php
/**
 * Displays archive pages if a speicifc template is not set.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/
 */
get_header(); ?>

        <div class="page-title-container">
            <h2>
				<?php
				if (is_front_page() && is_home()) {
					echo 'Default homepage';
				} elseif (is_front_page()) {
					echo 'static homepage';
				} elseif (is_home()) {
					echo 'blog page';
				} else {
					echo 'everyting else';
				}
				?> </h2>
        </div>
    <div class="content-wrapper">
        <div class="row">
            <main id="archive" class="main large-9 columns" role="main">
                <div class="archive-inner-content">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<?php get_template_part('parts/loop', 'archive'); ?>
				<?php endwhile; ?>
					<?php joints_page_navi(); ?>
				<?php else : ?>
					<?php get_template_part('parts/content', 'missing'); ?>
				<?php endif; ?>
                </div>
            </main>
			<?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>