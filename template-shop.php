<?php
/*
Template Name: Shop
*/
get_header(); ?>
<div class="shop-title-container">
    <h1>The Budget Savvy Bride Shop</h1>
</div>
<div class="content">
    <div class="inner-content">
        <main id="shop" class="main large-12 columns" role="main">
            <div class="row">
				<?php
				$args = array(
					'type' => 'shop_type',
					'taxonomy' => 'custom_cat-shop',
					'order' => 'ASC',
					'hide_empty' => '1');
				$categories = get_categories($args);
				if ($categories) {
					foreach ($categories as $category) {
						$link = get_term_link($category);
						echo '<div class="large-4 columns">';
						$attachment_id = get_field('post_category_image', 'category_' . $category->term_id);
						$image = get_field('post_category_image', 'category_' . $category->term_id);
						echo '<a href="' . $link . '">' . '<img src="' . $image['sizes']['article-thumbnail'] . '" />' . '<h2>' . $category->name . '</h2>' . '</a>';
						echo '</div>';
					}
				}
				?>
            </div>
        </main>
        <div class="clearfix"></div>
    </div> <!-- end #content -->
	<?php get_footer(); ?>
