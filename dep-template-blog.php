<?php
/*
Template Name: Blog
*/

get_header(); ?>

<div class="page-title-container">
	<h1>Blog</h1>

</div>

	<div class="content">

		<div class="row">

		    <main class="main large-9 columns" role="main">

	    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php get_template_part( 'parts/loop', 'archive' ); ?>

			<?php endwhile; ?>

				<?php joints_page_navi(); ?>

			<?php else : ?>

				<?php get_template_part( 'parts/content', 'missing' ); ?>

			<?php endif; ?>

		</main>

		<?php get_sidebar(); ?>

    </div>

</div>

<?php get_footer(); ?>