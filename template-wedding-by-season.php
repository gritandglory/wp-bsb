<?php
/*
Template Name: Wedding by Season
*/
get_header(); ?>
    <div class="page-title-container">
        <h1><?php single_post_title(); ?></h1>
    </div>
    <div class="content-wrapper">
        <div class="row">
            <main id="season-wedding" class="main small-12 large-9 columns" role="main">
                <div class="inner-content">


				<?php the_content(); ?>
                <div class="season-container">
					<?php
					$tag_name = 'Spring Weddings';
					$tag = 'spring-wedding';
					$args = array(
						'tag' => 'spring-wedding',
						'posts_per_page' => '3',
					);
					$the_query = new WP_Query($args);
                    $tag_id = $the_query->query_vars['tag_id'];

					?>
                    <h2 class="category-title"><?php echo $tag_name; ?></h2>
                    <p><?php echo tag_description( $tag_id ); ?> </p>
                    <div class="row">
	                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class="related_post medium-4 columns">
                                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
				                    <?php if (has_post_thumbnail()) : ?>
					                    <?php the_post_thumbnail('article-thumbnail') ?>
				                    <?php endif ?>
                                </a>
                                <h5 class="cat-title">
                                    <a class="cat-title-link" href="<?php $category = get_the_category();
                                    echo get_site_url() . '/category/' . $category[0]->slug; ?>">

			                            <?php $category = get_the_category();
			                            echo $category[0]->cat_name; ?>
                                    </a>

                                </h5>
                                <a href="<?php the_permalink() ?>">
                                    <h2 class="recent-post-title"><?php the_title(); ?></h2>
                                </a>
                            </div>
	                    <?php endwhile; ?>
                    </div>

                </div>
                <a class="btn btn-coral btn-medium-center" href="<?php echo site_url(); ?>/tag/<?php echo $tag; ?>">View
                    All</a>
				<?php wp_reset_postdata(); ?>
                <div class="season-container">
					<?php
					$tag_name = 'Summer Weddings';
					$tag = 'summer-wedding';
					$args = array(
						'tag' => 'summer-wedding',
						'posts_per_page' => '3',
					);
					$the_query = new WP_Query($args);
                    $tag_id = $the_query->query_vars['tag_id'];
					?>
                    <h2 class="category-title"><?php echo $tag_name; ?></h2>
                    <p><?php echo tag_description( $tag_id ); ?> </p>
                    <div class="row">
					<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="related_post medium-4 columns">
                            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
								<?php if (has_post_thumbnail()) : ?>
									<?php the_post_thumbnail('article-thumbnail') ?>
								<?php endif ?>
                            </a>
                            <h5 class="cat-title">
                                <a class="cat-title-link" href="#">
			                        <?php $category = get_the_category();
			                        echo $category[0]->cat_name; ?>
                                </a>

                            </h5>
                            <a href="<?php the_permalink() ?>">
                                <h2 class="recent-post-title"><?php the_title(); ?></h2>
                            </a>
                        </div>
					<?php endwhile; ?>
                    </div>
                </div>
                <a class="btn btn-coral btn-medium-center" href="<?php echo site_url(); ?>/tag/<?php echo $tag; ?>">View
                    All</a>
				<?php wp_reset_postdata(); ?>
                <div class="season-container">
					<?php
					$tag_name = 'Fall Weddings';
					$tag = 'fall-wedding';
					$args = array(
						'tag' => 'fall-wedding',
						'posts_per_page' => '3',
					);
					$the_query = new WP_Query($args);
                    $tag_id = $the_query->query_vars['tag_id'];
					?>
                    <h2 class="category-title"><?php echo $tag_name; ?></h2>
                    <p><?php echo tag_description( $tag_id ); ?> </p>
                    <div class="row">
	                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class="related_post medium-4 columns">
                                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
				                    <?php if (has_post_thumbnail()) : ?>
					                    <?php the_post_thumbnail('article-thumbnail') ?>
				                    <?php endif ?>
                                </a>
                                <h5 class="cat-title">
                                    <a class="cat-title-link" href="#">
			                            <?php $category = get_the_category();
			                            echo $category[0]->cat_name; ?>
                                    </a>

                                </h5>
                                <a href="<?php the_permalink() ?>">
                                    <h2 class="recent-post-title"><?php the_title(); ?></h2>
                                </a>
                            </div>
	                    <?php endwhile; ?>
                    </div>

                </div>
                <a class="btn btn-coral btn-medium-center" href="<?php echo site_url(); ?>/tag/<?php echo $tag; ?>">View
                    All</a>
				<?php wp_reset_postdata(); ?>
                <div class="season-container">
					<?php
					$tag_name = 'Winter Weddings';
					$tag = 'winter-wedding';
					$args = array(
						'tag' => 'winter-wedding',
						'posts_per_page' => '3',
					);
					$the_query = new WP_Query($args);
                    $tag_id = $the_query->query_vars['tag_id'];
					?>
                    <h2 class="category-title"><?php echo $tag_name; ?></h2>
                    <p><?php echo tag_description( $tag_id ); ?> </p>
                    <div class="row">
	                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class="related_post medium-4 columns">
                                <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
				                    <?php if (has_post_thumbnail()) : ?>
					                    <?php the_post_thumbnail('article-thumbnail') ?>
				                    <?php endif ?>
                                </a>
                                <h5 class="cat-title">
                                    <a class="cat-title-link" href="#">
			                            <?php $category = get_the_category();
			                            echo $category[0]->cat_name; ?>
                                    </a>

                                </h5>
                                <h2>
                                    <a href="<?php the_permalink() ?>">
                                  <?php the_title(); ?>
                                </a>
                                </h2>
                            </div>
	                    <?php endwhile; ?>
                    </div>

                </div>
                <a class="btn btn-coral btn-medium-center" href="<?php echo site_url(); ?>/tag/<?php echo $tag; ?>">View
                    All</a>
				<?php wp_reset_postdata(); ?>
                <div class="clearfix"></div>
                </div>
            </main>
			<?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>