<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */
get_header(); ?>
    <div class="page-title-container">
        <h2><?php single_post_title(); ?></h2>
    </div>
    <div class="content-wrapper">
        <div class="row">
            <main id="page" class="main small-12 large-9 columns" role="main">
                <div class="inner-content">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<?php get_template_part('parts/loop', 'page'); ?>
					<?php endwhile; endif; ?>
                </div>
            </main>
			<?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>